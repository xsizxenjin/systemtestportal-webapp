{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}
{{define "testprotocol"}}


{{if .ActivityItem.IsCase}}
    {{if .ActivityItem.CaseExecutionProtocol}}
        <li id="{{ .ActivityItem.CaseExecutionProtocol.Result.Integer }}">
    {{else}}
        <li>
    {{end}}
{{else}}
    {{if .ActivityItem.SequenceExecutionProtocol}}
        <li  id="{{ .ActivityItem.SequenceExecutionProtocol.Result.Integer }}">
    {{else}}
        <li>
    {{end}}
{{end}}

     <!-------------------------------------------------- BADGE ----------------------------------------------->
    {{if .ActivityItem.IsCase}}
        {{if .ActivityItem.CaseExecutionProtocol}}
                <!-- NotAssessed -->
            {{if eq .ActivityItem.CaseExecutionProtocol.Result 0}}
                <div class="timeline-badge info cursor-clickable"
                     onclick="DataTree.onUserClickedBadge(
                     {{if .ActivityItem.IsCase}}
                        {{ .ActivityItem.CaseExecutionProtocol.Result.Integer }}
                        {{else}}
                            {{ .ActivityItem.SequenceExecutionProtocol.Result.Integer }}
                        {{end}})"
                >

                    <i class="fa fa-question text-white"></i>
                </div>
            {{end}}
                <!-- Pass -->
            {{if eq .ActivityItem.CaseExecutionProtocol.Result 1}}
                <div class="timeline-badge success cursor-clickable"
                     onclick="DataTree.onUserClickedBadge(
                     {{if .ActivityItem.IsCase}}
                        {{ .ActivityItem.CaseExecutionProtocol.Result.Integer }}
                        {{else}}
                            {{ .ActivityItem.SequenceExecutionProtocol.Result.Integer }}
                        {{end}})"
                >
                    <i class="fa fa-check text-white"></i>
                </div>
            {{end}}
                <!-- Partially Pass -->
            {{if eq .ActivityItem.CaseExecutionProtocol.Result 2}}
                <div class="timeline-badge warning cursor-clickable"
                     onclick="DataTree.onUserClickedBadge(
                     {{if .ActivityItem.IsCase}}
                        {{ .ActivityItem.CaseExecutionProtocol.Result.Integer }}
                        {{else}}
                            {{ .ActivityItem.SequenceExecutionProtocol.Result.Integer }}
                        {{end}})"
                >
                    <i class="fa fa-exclamation text-white"></i>
                </div>
            {{end}}
                <!-- Fail -->
            {{if eq .ActivityItem.CaseExecutionProtocol.Result 3}}
                <div class="timeline-badge danger cursor-clickable"
                     onclick="DataTree.onUserClickedBadge(
                     {{if .ActivityItem.IsCase}}
                        {{ .ActivityItem.CaseExecutionProtocol.Result.Integer }}
                        {{else}}
                            {{ .ActivityItem.SequenceExecutionProtocol.Result.Integer }}
                        {{end}})"
                >
                    <i class="fa fa-exclamation text-white"></i>
                </div>
            {{end}}
                <!-- Not Applicable -->
            {{if eq .ActivityItem.CaseExecutionProtocol.Result 4}}
                <div class="timeline-badge info cursor-clickable"
                     onclick="DataTree.onUserClickedBadge(
                     {{if .ActivityItem.IsCase}}
                        {{ .ActivityItem.CaseExecutionProtocol.Result.Integer }}
                        {{else}}
                            {{ .ActivityItem.SequenceExecutionProtocol.Result.Integer }}
                        {{end}})"
                >
                    <i class="fa fa-minus text-white"></i>
                </div>
            {{end}}
        {{ else }}
            <div class="timeline-badge info cursor-clickable" onclick="DataTree.onUserClickedBadge(5)">
                <i class="fa fa-info fa-lg"></i>
            </div>
        {{ end }}
    {{else}}

        {{ if .ActivityItem.SequenceExecutionProtocol}}
                <!-- NotAssessed -->
            {{if eq .ActivityItem.SequenceExecutionProtocol.Result 0}}
                <div class="timeline-badge info cursor-clickable"
                     onclick="DataTree.onUserClickedBadge(
                     {{if .ActivityItem.IsCase}}
                    {{ .ActivityItem.CaseExecutionProtocol.Result.Integer }}
                    {{else}}
                        {{ .ActivityItem.SequenceExecutionProtocol.Result.Integer }}
                    {{end}})"
                >
                    <i class="fa fa-question text-white"></i>
                </div>
            {{end}}
                <!-- Pass -->
            {{if eq .ActivityItem.SequenceExecutionProtocol.Result 1}}
                <div class="timeline-badge success cursor-clickable"
                     onclick="DataTree.onUserClickedBadge(
                     {{if .ActivityItem.IsCase}}
                    {{ .ActivityItem.CaseExecutionProtocol.Result.Integer }}
                    {{else}}
                        {{ .ActivityItem.SequenceExecutionProtocol.Result.Integer }}
                    {{end}})"
                >
                    <i class="fa fa-check text-white"></i>
                </div>
            {{end}}
                <!-- Partially Pass -->
            {{if eq .ActivityItem.SequenceExecutionProtocol.Result 2}}
                <div class="timeline-badge warning cursor-clickable"
                     onclick="DataTree.onUserClickedBadge(
                     {{if .ActivityItem.IsCase}}
                    {{ .ActivityItem.CaseExecutionProtocol.Result.Integer }}
                    {{else}}
                        {{ .ActivityItem.SequenceExecutionProtocol.Result.Integer }}
                    {{end}})"
                >
                    <i class="fa fa-exclamation text-white"></i>
                </div>
            {{end}}
                <!-- Fail -->
            {{if eq .ActivityItem.SequenceExecutionProtocol.Result 3}}
                <div class="timeline-badge danger cursor-clickable"
                     onclick="DataTree.onUserClickedBadge(
                     {{if .ActivityItem.IsCase}}
                    {{ .ActivityItem.CaseExecutionProtocol.Result.Integer }}
                    {{else}}
                        {{ .ActivityItem.SequenceExecutionProtocol.Result.Integer }}
                    {{end}})"
                >
                    <i class="fa fa-exclamation text-white"></i>
                </div>
            {{end}}
                <!-- Not Applicable -->
            {{if eq .ActivityItem.SequenceExecutionProtocol.Result 4}}
                <div class="timeline-badge info cursor-clickable"
                     onclick="DataTree.onUserClickedBadge(
                     {{if .ActivityItem.IsCase}}
                    {{ .ActivityItem.CaseExecutionProtocol.Result.Integer }}
                    {{else}}
                        {{ .ActivityItem.SequenceExecutionProtocol.Result.Integer }}
                    {{end}})"
                >
                    <i class="fa fa-minus text-white"></i>
                </div>
            {{end}}
        {{ else }}
            <div class="timeline-badge info cursor-clickable" onclick="DataTree.onUserClickedBadge(5)">
                <i class="fa fa-info fa-lg"></i>
            </div>
        {{ end }}
    {{end}}

    <!-------------------------------------------------- TIMELINE PANEL ----------------------------------------------->
        <div class="timeline-panel">

            <div class="timeline-heading">

            {{if .Activity.Author }}
                {{ if and (not .Activity.Author) .SystemSettings.IsDeleteUsers }}
                <a href="#" class="pull-left mr-3">
                <img src="{{getImagePath .Activity.Author.Image "user"}}" alt="" class="rounded-circle profile-picture" height="42" width="42">
                </a>
                {{ else }}
                <a href="/users/{{.Activity.Author.Name}}" class="pull-left mr-3">
                    <img src="{{getImagePath .Activity.Author.Image "user"}}" alt="" class="rounded-circle profile-picture" height="42" width="42">
                </a>
                {{ end }}
            {{ end }}

                <!-- Top Left -->
                <div class="media-body w-100">
                    {{ if and (not .Activity.Author) .SystemSettings.IsDeleteUsers }}
                        <strong class=""> {{T "Anonymous" .}} </strong>
                    {{ else }} 
                        <strong class=""> {{ .Activity.Author.DisplayName }} </strong>
                    {{ end }}
                    {{T "executed the" . }}

                    {{if .Activity.ActivityItem.IsCase}}
                        {{T "test case" . }}
                    {{ else }}
                        {{T "test sequence" .}}
                    {{end}}

                    {{if .Activity.ActivityItem.IsCase}}

                        {{if .Activity.ActivityItem.TestCase}}
                            <a href="#"
                               onclick="DataTree.onUserClickedTest({{ .Activity.ActivityItem.TestCase.Name}}, false);return false;">
                            {{ .Activity.ActivityItem.TestCase.Name}}
                            </a>
                        {{else}}
                            <em class="text-muted activity-tooltip" data-toggle="tooltip" data-placement="top" title="The executed test case has been deleted"> unknown test case</em>
                        {{end}}

                    {{ else }}
                        {{if .Activity.ActivityItem.TestSequence}}
                            <a href="#"
                               onclick="DataTree.onUserClickedTest({{ .Activity.ActivityItem.TestSequence.Name}}, true);return false;">
                            {{ .Activity.ActivityItem.TestSequence.Name}}
                            </a>
                        {{else}}
                            <em class="text-muted activity-tooltip" data-toggle="tooltip" data-placement="top" title="The executed test sequence has been deleted"> unknown test sequence</em>
                        {{end}}
                    {{end}}

                    <div >
                        <div class="col-sm-8 activity-tooltip" data-toggle="tooltip" data-placement="bottom" title="{{ .Activity.ActivityItem.UpdatedAt }}">
                            <small class="text-muted"><i class="fa fa-clock-o fa-lg"></i></small>
                            <time class="timeago text-muted small align-middle " datetime="{{ provideTimeago .Activity.ActivityItem.UpdatedAt }}"></time>
                        </div>
                    </div>
                </div>
            </div>

    <!-------------------------------------------------- TIMELINE BODY- ----------------------------------------------->
            <div class="timeline-body">
                <div class="card" style="margin-top: 1rem;">
                    <div class="card-header d-flex flex-row" id="heading-{{.Id}}">

                        <div class="pl-2">
                        {{if .Activity.ActivityItem.IsCase}}
                            <!--We dont' have to check if the case is nil, cus if the case is nil the exec protocol is also nil -->
                            {{if .Activity.ActivityItem.CaseExecutionProtocol }}
                                    {{if .Activity.ActivityItem.TestCase}}
                                        <p class="m-0">
                                            <a href="#"
                                               onclick="DataTree.onUserClickedProtocol({{ .Activity.ActivityItem.TestCase.Name}}, {{ .Activity.ActivityItem.CaseExecutionProtocol.ProtocolNr}}, false);return false;">
                                                {{T "View the test protocol" . }}
                                                <small class="text-muted"><i class="fa fa-external-link"></i></small>
                                            </a>
                                        </p>
                                    {{else}}
                                        <em class="text-muted activity-tooltip" data-toggle="tooltip" data-placement="bottom" title="The test case has been deleted"> Unknown protocol</em>
                                    {{end}}
                            {{else}}
                                <em class="text-muted activity-tooltip" data-toggle="tooltip" data-placement="bottom" title="The test protocol has been deleted"> Unknown protocol</em>
                            {{end}}
                        {{ else }}
                            <!--We dont' have to check if the sequence is nil, cus if the sequence is nil the exec protocol is also nil -->
                            {{if .Activity.ActivityItem.SequenceExecutionProtocol}}
                                {{if .Activity.ActivityItem.TestSequence }}
                                    <p class="m-0">
                                        <a href="#"
                                           onclick="DataTree.onUserClickedProtocol({{ .Activity.ActivityItem.TestSequence.Name}}, {{ .Activity.ActivityItem.SequenceExecutionProtocol.ProtocolNr}}, true);return false;">
                                            {{T "View the test protocol" .}}
                                            <small class="text-muted"><i class="fa fa-external-link"></i></small>
                                        </a>
                                    </p>
                                {{else}}
                                    <em class="text-muted activity-tooltip" data-toggle="tooltip" data-placement="bottom" title="The test sequence has been deleted"> Unknown protocol</em>
                                {{end}}
                            {{else}}
                                <em class="text-muted activity-tooltip" data-toggle="tooltip" data-placement="bottom" title="The test protocol has been deleted"> Unknown protocol</em>
                            {{end}}
                        {{end}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
{{ end }}

