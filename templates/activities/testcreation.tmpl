{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}
{{define "testcreation"}}

<li id="5">

    <!-------------------------------------------------- BADGE ----------------------------------------------->
    <div class="timeline-badge info cursor-clickable" onclick="DataTree.onUserClickedBadge(5)">
        <i class="fa fa-info fa-lg"></i>
    </div>

    <!-------------------------------------------------- TIMELINE PANEL ----------------------------------------------->
    <div class="timeline-panel">

        <div class="timeline-heading">

            <!-- Image -->
            {{if .Activity.Author }}
                {{ if and (not .Activity.Author) .SystemSettings.IsDeleteUsers }}
                <a href="#" class="pull-left mr-3">
                <img src="{{getImagePath .Activity.Author.Image "user"}}" alt="" class="rounded-circle profile-picture" height="42" width="42">
                </a>
                {{ else }}
                <a href="/users/{{.Activity.Author.Name}}" class="pull-left mr-3">
                    <img src="{{getImagePath .Activity.Author.Image "user"}}" alt="" class="rounded-circle profile-picture" height="42" width="42">
                </a>
                {{ end }}
            {{ end }}

            <!-- Top Left -->
            <div class="media-body w-100">

                {{ if and (not .Activity.Author) $.SystemSettings.IsDeleteUsers }}
                    <strong class=""> {{T "Anonymous" .}} </strong>
                {{ else }} 
                    <strong class=""> {{ .Activity.Author.DisplayName }} </strong>
                {{ end }}

                {{T "created a new" .}}

                {{if .Activity.ActivityItem.IsCase}}
                {{T "Test Case" . }}
                {{ else }}
                {{T "Test Sequence" . }}
                {{end}}

                <div >
                    <div class="col-sm-8 activity-tooltip" data-toggle="tooltip" data-placement="bottom" title="{{ .Activity.ActivityItem.UpdatedAt }}">
                        <small class="text-muted"><i class="fa fa-clock-o fa-lg"></i></small>
                        <time class="timeago text-muted small align-middle " datetime="{{ provideTimeago .Activity.ActivityItem.UpdatedAt }}"></time>
                    </div>
                </div>
            </div>

        </div>

    <!-------------------------------------------------- TIMELINE BODY- ----------------------------------------------->
        <div class="timeline-body">
            <div class="card" style="margin-top: 1rem;">
                <div class="card-header d-flex flex-row" id="heading-{{.Activity.Id}}">

                    <div class="pl-2">
                        {{if .Activity.ActivityItem.IsCase}}
                            {{ if .Activity.ActivityItem.TestCase}}
                                <p class="m-0">
                                    <a href="#"
                                       onclick="DataTree.onUserClickedTest({{ .Activity.ActivityItem.TestCase.Name}}, false);return false;">
                                    {{ .Activity.ActivityItem.TestCase.Name}}
                                        <small class="text-muted"><i class="fa fa-external-link"></i></small>
                                    </a>
                                </p>
                            {{else}}
                                <em class="text-muted activity-tooltip" data-toggle="tooltip" data-placement="bottom" title="The created test case has been deleted">unknown test case</em>
                            {{end}}
                        {{ else }}
                            {{if .Activity.ActivityItem.TestSequence}}
                                <p class="m-0">
                                    <a href="#"
                                       onclick="DataTree.onUserClickedTest({{ .Activity.ActivityItem.TestSequence.Name}}, true);return false;">
                                    {{ .Activity.ActivityItem.TestSequence.Name}}
                                        <small class="text-muted"><i class="fa fa-external-link"></i></small>
                                    </a>
                                </p>
                            {{else}}
                                <em class="text-muted activity-tooltip" data-toggle="tooltip" data-placement="bottom" title="The created test sequence has been deleted">unknown test sequence</em>
                            {{end}}
                        {{end}}
                    </div>
                 </div>
            </div>
        </div>
    </div>

</li>
{{ end }}