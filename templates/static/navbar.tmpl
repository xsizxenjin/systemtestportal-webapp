{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

<!-- Translated to German -->
{{define "navbar"}}
<!-- Message Navbar -->
{{if .SystemSettings.IsDisplayGlobalMessage }}
<nav id="system-message" class="navbar navbar-expand-sm navbar-dark fixed-top navbar-lower {{.SystemSettings.GlobalMessageType}}" style="display: none;">
    <span class="navbar-message-text">
        {{printMarkdown .SystemSettings.GlobalMessage}}
    </span>
    <div class="pull-right">
        <ul class="nav navbar-nav navbar-message-button">
            <li><button type="submit" style="padding: 10px" id="remove-system-message" class="btn navbar-message-button-close btn-default">
                <img src="/static/img/icon_close.png">
            </button></li>
        </ul>
    </div>
</nav>
<script src="/static/js/util/common.js" integrity="{{sha256 "/static/js/util/common.js"}}"></script>
<script>messageTimeStamp = {{.SystemSettings.GlobalMessageTimeStamp}};</script>
<script>globalMessageExpires = {{.SystemSettings.IsGlobalMessageExpirationDate}};</script>
<script>globalMessageExpiration = {{.SystemSettings.GlobalMessageExpirationDateUnix}};</script>
<script src="/static/js/system/message.js" integrity="{{sha256 "/static/js/system/message.js"}}"></script>
{{end}}

<!-- Main Navbar -->
<nav class="navbar navbar-expand-sm navbar-dark  bg-dark fixed-top">

    <div class="navbar-brand">
        <img class="No-Warning-On-Current-Action-Abort rounded" src="/static/img/STP-Logo-small.png"  width="30" height="30" alt="">
    </div>

    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarCollapse">

		<ul class="navbar-nav mr-auto">

            {{if or .SystemSettings.IsAccessAllowed .SignedIn}}
                <li class="nav-item">
                    <a class="nav-link" href="/explore">{{T "Projects" .}}</a>
			    </li>
                <li class="nav-item">
                    <a class="nav-link" href="/explore/globaldashboard">{{T "Dashboard" .}}</a>
                </li>
            {{end}}

		</ul>

        <ul class="navbar-nav mt-md-0">



        {{ if .SignedIn }}

            {{if .User.IsAdmin}}

                <li class="nav-item">
                    <a class="nav-link" href="/register" data-toggle="tooltip" data-placement="bottom"
                       title="Register a new user">
                        <i class="fa fa-user-plus fa-lg"></i>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/settings" data-toggle="tooltip" data-placement="bottom"
                       title="Admin Settings">
                        <i class="fa fa-cog fa-lg"></i>
                    </a>
                </li>

            {{end}}

            <li class="nav-item">
                <a class="nav-link

                    {{ if not (eq .TaskCount -1) }}
                        pr-0
                    {{end}}" href="/dashboard/tasks/opened" data-toggle="tooltip" data-placement="bottom" title="Tasks">

                    <i class="fa fa-tasks fa-lg"></i>

                    {{ if not (eq .TaskCount -1) }}
                        <span class="badge badge-pill badge-primary mr-0" style="position:relative; top: -8px; left: -9px;">
                            {{ .TaskCount }}
                        </span>
                    {{ end }}
                </a>
            </li>

            <li class="nav-item dropdown pt-0">

                <a class="nav-link dropdown-toggle No-Warning-On-Current-Action-Abort" href="./{{.User.ID.Actor}}" id="userMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="{{getImagePath .User.Image "user"}}" alt="..." class="rounded-circle No-Warning-On-Current-Action-Abort profile-picture" height="26" width="26">
                </a>

                <div class="dropdown-menu pull-left" id="userMenuDropdown" aria-labelledby="userMenu">

                    <span class="dropdown-item disabled">
                        <strong>{{.User.DisplayName}}</strong>
                    </span>

                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/users/{{.User.Name}}">{{T "Profile" .}}</a>
                    <a class="dropdown-item" href="/users/{{.User.Name}}/settings">{{T "Settings" .}}</a>
                    <a class="dropdown-item" href="javascript: signout()">
                        <strong>{{T "Sign out" .}}</strong>
                    </a>
                </div>

            </li>

        {{ else }}

            {{if .SystemSettings.IsRegistrationAllowed}}
                <li class="nav-item">
                    <a class="nav-link" href="/register" data-toggle="tooltip" data-placement="bottom"
                       title="Register yourself">
                        <i class="fa fa-user-plus fa-lg"></i>
                    </a>
                </li>
            {{end}}

            <li class="nav-item" id="signin-link" data-trigger="manual" data-toggle="tooltip" data-placement="left" data-template='<div class="tooltip tooltip-inverted" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                title="" data-original-title={{T "Sign in to proceed!" .}}>
                <a class="nav-link" data-toggle="modal" data-target="#signin-modal" href="">
                    {{T "Sign in" .}}
                </a>
            </li>

        {{ end }}

		</ul>
	</div>
</nav>

<div id="modalPlaceholder"></div>
{{template "modal-signin"}}

<!-- Import Scripts here -->
<script src="/static/js/misc/sign-out.js" integrity="{{sha256 "/static/js/misc/sign-out.js"}}"></script>
{{end}}