{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "header"}}
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Landing Page">
        <meta name="author" content="guertlms">

    <title>System Test Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="/static/libraries/bootstrap-4.1.3-dist/css/bootstrap.min.css" integrity="{{sha256 "/static/libraries/bootstrap-4.1.3-dist/css/bootstrap.min.css"}}" rel="stylesheet" type="text/css">

        <!-- Custom styles for this template -->
        <link href="/static/css/stp-custom.css" integrity="{{sha256 "/static/css/stp-custom.css"}}" rel="stylesheet" type="text/css">

        <!-- Icons -->
        <link href="/static/css/font-awesome.min.css" integrity="{{sha256 "/static/css/font-awesome.min.css"}}" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="/static/assets/js/vendor/jquery-3.2.1.min.js" integrity="{{sha256 "/static/assets/js/vendor/jquery-3.2.1.min.js"}}"></script>

        <!-- jQuery UI -->
        <script src="/static/libraries/jquery-sortable/js/jquery-ui.min.js" integrity="{{sha256 "/static/libraries/jquery-sortable/js/jquery-ui.min.js"}}"></script>

        <!-- Bootstrap-toggle-master -->
        <link href="/static/libraries/bootstrap-toggle-master/css/bootstrap-toggle.min.css" integrity="{{sha256 "/static/libraries/bootstrap-toggle-master/css/bootstrap-toggle.min.css"}}" rel="stylesheet" type="text/css">

        <!-- Bootstrap-multiselect -->
        <link href="/static/libraries/bootstrap-multiselect/css/bootstrap-multiselect.css" integrity="{{sha256 "/static/libraries/bootstrap-multiselect/css/bootstrap-multiselect.css"}}" rel="stylesheet"
              type="text/css">

        <!-- DataTable -->
        <link rel="stylesheet" type="text/css" href="/static/libraries/DataTables/datatables.min.css" integrity="{{sha256 "/static/libraries/DataTables/datatables.min.css"}}"/>
        <script type="text/javascript" src="/static/libraries/DataTables/datatables.min.js"></script>

        <!-- favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/static/img/favicons/apple-touch-icon.png" integrity="{{sha256 "/static/img/favicons/apple-touch-icon.png"}}">
        <link rel="icon" type="image/png" sizes="32x32" href="/static/img/favicons/favicon-32x32.png" integrity="{{sha256 "/static/img/favicons/favicon-32x32.png"}}">
        <link rel="icon" type="image/png" sizes="16x16" href="/static/img/favicons/favicon-16x16.png" integrity="{{sha256 "/static/img/favicons/favicon-16x16.png"}}">
        <link rel="manifest" href="/static/img/favicons/manifest.json" integrity="{{sha256 "/static/img/favicons/manifest.json"}}">
        <link rel="shortcut icon" href="/static/img/favicons/favicon.ico" integrity="{{sha256 "/static/img/favicons/favicon.ico"}}">
        <meta name="msapplication-config" content="/static/img/favicons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">

        <!-- timeAgo -->
        <script src="/static/assets/js/jquery.timeago.js" type="text/javascript" integrity="{{sha256 "/static/assets/js/jquery.timeago.js"}}"></script>

        <!-- navbar css fix -->
        <link href="/static/css/navbar-top-fixed.css" integrity="{{sha256 "/static/css/navbar-top-fixed.css"}}" rel="stylesheet" type="text/css">

        <!--JQUI -->
        <link rel="stylesheet" href="/static/css/jquery-ui.css" integrity="{{sha256 "/static/css/jquery-ui.css"}}">
        <script src="/static/js/util/jquery-ui.js" integrity="{{sha256 "/static/js/util/jquery-ui.js"}}"></script>

    </head>

    <body>
            {{template "navbar" .}}
            <div id="mainContent" class="container-fluid">
                <div class="row justify-content-center">
                {{template "content-main" .}}
                </div>
            </div>
        {{template "footer" .}}
    </body>
</html>

<!-- Placed at the end of the document so the pages load faster -->
<!-- Bootstrap Core Scripts -->
<script src="/static/assets/js/vendor/popper.min.js" integrity="{{sha256 "/static/assets/js/vendor/popper.min.js"}}"></script>
<script src="/static/libraries/bootstrap-4.1.3-dist/js/bootstrap.min.js" integrity="{{sha256 "/static/libraries/bootstrap-4.1.3-dist/js/bootstrap.min.js"}}"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/static/assets/js/ie10-viewport-bug-workaround.js" integrity="{{sha256 "/static/assets/js/ie10-viewport-bug-workaround.js"}}"></script>

<!-- Bootstrap-toggle-master -->
<script src="/static/libraries/bootstrap-toggle-master/js/bootstrap-toggle.min.js" integrity="{{sha256 "/static/libraries/bootstrap-toggle-master/js/bootstrap-toggle.min.js"}}"></script>

<!-- Bootstrap-multiselect -->
<script type="text/javascript" src="/static/libraries/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>

<!-- Browser History Functionality -->
<script>

    jQuery(document).ready(function ($) {
        if (window.history && window.history.pushState) {
            $(window).on('popstate', function () {
                window.location.reload();
            });
        }
    });
    <!-- Remove potential whitespace used for indentation from textareas -->
    $('document').ready(function () {
        $('textarea').each(function () {
                    $(this).val($(this).val().trim());
                }
        );
        // wipe sessionStorage so saved filters don't cause issues
        sessionStorage.clear();
    });
</script>

<!-- Internal History Functionality -->
<script type="text/javascript" src="/static/js/util/historySTP.js"></script>
{{end}}