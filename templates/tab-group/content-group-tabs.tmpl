{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "content"}}
<div class="row d-sm-flex d-none mt-2 mb-4">
    <div class="col-sm-2 d-none d-sm-block d-print-none">
        <div class="d-flex project-small-thumbnail" id="projectImage">
            <img src="/static/img/placeholder.svg" class="rounded" alt="project logo">
        </div>
    </div>
    <div class="col-xs-12 col-sm-10">
        <div class="card-block">
            <div class="d-none float-right badge badge-secondary d-print-block"><span>{{T "created" .}} </span>{{ .Group.CreationDate }}</div>
            <div class="d-none d-sm-block float-right badge badge-secondary d-print-none"><span>{{T "created" .}} </span><time class="timeago" datetime="">{{ .Group.CreationDate }}</time></div>
            <h4 class="card-title">{{ .Group.Name }}</h4>
            <p class="card-text font-italic">{{ .Group.Description }}</p>
        </div>
    </div>
</div>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" id="tabButtonActivity" href="" role="tab">{{T "Activity" .}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link disabled" data-toggle="tab" id="tabButtonProjects" href="" role="tab">{{T "Projects" .}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link disabled" data-toggle="tab" id="tabButtonMembers" href="" role="tab">{{T "Members" .}}</a>
    </li>
{{ if or .Member .Admin }}
    <li class="nav-item ml-auto">
        <a class="nav-link disabled" data-toggle="tab" id="tabButtonSettings" href="" role="tab">{{T "Settings" .}}</a>
    </li>
    {{ end }}
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="tabarea" role="tabpanel">
        {{template "tab-content" . }}
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/group/group-tabs.js" integrity="{{sha256 "/static/js/group/group-tabs.js"}}"></script>

<script>
    initializeGroupTabClickListener();
</script>
{{end}}


