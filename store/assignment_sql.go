/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package store

import (
	"encoding/json"
	"net/http"

	"github.com/go-xorm/xorm"
	"github.com/pkg/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity/activityItems"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

type AssignmentSQL struct {
	engine *xorm.Engine
}

func (assignmentSQL AssignmentSQL) Create(session *xorm.Session, activityType int, activityEntities *activity.ActivityEntities, request *http.Request) (int64, error) {

	newAssignment := &activityItems.Assignment{}

	switch activityType {
	case activity.ASSIGN_CASE:
		newAssignment.TestId = activityEntities.Case.Id
		newAssignment.IsCase = true
	case activity.ASSIGN_SEQUENCE:
		newAssignment.TestId = activityEntities.Sequence.Id
		newAssignment.IsCase = false
	default:
		return 0, errors.Errorf("%s", "Could not find the specified assignment type")
	}

	_, err := session.Insert(newAssignment)
	if err != nil {
		return 0, err
	}

	// Get the assignee structs
	assignedUsers, err := getAssignedUsers(newAssignment, request)
	if err != nil {
		return 0, err
	}

	// Insert the Assignees into the DB
	for _, assignedUser := range assignedUsers {
		_, err = session.Insert(assignedUser)
		if err != nil {
			return 0, err
		}
	}

	return newAssignment.Id, nil
}

func getAssignedUsers(assignment *activityItems.Assignment, request *http.Request) ([]*activityItems.AssignedUsers, error) {
	assigneeIds := []string{}
	assignedUsers := []*activityItems.AssignedUsers{}

	assigneeIdsRaw := request.FormValue(httputil.NewTesters)

	err := json.Unmarshal([]byte(assigneeIdsRaw), &assigneeIds)
	if err != nil {
		return nil, err
	}

	for _, assigneeId := range assigneeIds {
		assignedUsers = append(assignedUsers, &activityItems.AssignedUsers{AssignmentId: assignment.Id, AssigneeId: assigneeId})
	}

	return assignedUsers, nil
}

func (assignmentSQL AssignmentSQL) GetLazyLoadedActivityItem(activity *activity.Activity) (activity.ActivityItem, error) {
	userStore := GetUserStore()
	assignment := activityItems.Assignment{}

	// First load the assignment
	has, err := assignmentSQL.engine.Where("id = ?", activity.ActivityItemId).Get(&assignment)
	if err != nil {
		return nil, err
	} else if !has {
		err = errors.Errorf("%s%v%s", "The Activity with ID: ", activity.ActivityItemId, " has no activity-item")
		return nil, err
	}

	// Load the Assignees --------------------------------------------------------------------------|
	// First get the many2many relations
	assignedUsers := []*activityItems.AssignedUsers{}

	err = assignmentSQL.engine.Where("assignment_id = ?", assignment.Id).Find(&assignedUsers)
	if err != nil {
		return nil, err
	}

	// then get the actual users out of them
	assignees := []*user.User{}
	for _, assignedUser := range assignedUsers {
		user, _, err := userStore.Get(id.NewActorID(assignedUser.AssigneeId))
		if err != nil {
			return nil, err
		}

		assignees = append(assignees, user)
	}

	assignment.Assignees = assignees
	// ---------------------------------------------------------------------------------------------|
	// Load the Test -------------------------------------------------------------------------------|
	if assignment.IsCase {
		testCase := &test.Case{}
		has, err := assignmentSQL.engine.Table("test_cases").Where("id = ?", assignment.TestId).Get(testCase)
		if err != nil {
			return nil, err
		} else if !has {
			// If the test case has been deleted, just give the template nil
			testCase = nil
		}

		assignment.TestCase = testCase
	} else {
		testSequence := &test.Sequence{}
		has, err := assignmentSQL.engine.Table("test_sequences").Where("id = ?", assignment.TestId).Get(testSequence)
		if err != nil {
			return nil, err
		} else if !has {
			// If the test sequence has been deleted, just give the template nil
			testSequence = nil
		}

		assignment.TestSequence = testSequence
	}
	// ---------------------------------------------------------------------------------------------|

	// Move the Assignment into the Activity as the Activityitem

	return assignment, nil
}
