/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package store

import (
	"github.com/go-xorm/xorm"
	"github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/integration"
)

type gitlabIssueRow struct {
	Id         int64
	ProtocolId int64
	GitlabId   int
}

func (psSQL ProjectsSQL) AddIssue(issue integration.GitLabIssue) error {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return err
	}

	err := addIssue(s, issue)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func addIssue(s *xorm.Session, issue integration.GitLabIssue) error {

	issueRow, err := gitlabIssueRowFromGitlabIssue(s, issue)
	if err != nil {
		return err
	}

	_, err = s.Table(gitlabIssueTable).Insert(issueRow)
	if err != nil {
		return err
	}

	return nil
}

func gitlabIssueRowFromGitlabIssue(s *xorm.Session, issue integration.GitLabIssue) (*gitlabIssueRow, error) {
	var caseProtocolRows []caseProtocolRow
	rows, err := getCaseProtocolRows(s, issue.Protocol.TestVersion.TestID)
	if err != nil {
		return nil, err
	}
	caseProtocolRows = rows

	var protocolRowID int64
	for _, protocolRow := range caseProtocolRows {
		if protocolRow.ProtocolNr == issue.Protocol.ProtocolNr {
			protocolRowID = protocolRow.Id
			break
		}
	}

	return &gitlabIssueRow{
		ProtocolId: protocolRowID,
		GitlabId:   issue.GitLabID,
	}, nil
}

func (psSQL ProjectsSQL) GetIssue(testCaseID id.TestID, version string, variant string) (*integration.GitLabIssue, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	var gitlabIssueRows []gitlabIssueRow
	err := s.Table(gitlabIssueTable).Find(&gitlabIssueRows)
	if err != nil {
		return nil, err
	}

	for _, issueRow := range gitlabIssueRows {
		issue, err := gitlabIssueFromGitlabIssueRow(s, issueRow, testCaseID)
		if err != nil {
			return nil, err
		}
		if issue == nil {
			continue
		}
		if issue.Protocol.TestVersion.TestID == testCaseID &&
			issue.Protocol.SUTVersion == version &&
			issue.Protocol.SUTVariant == variant {
				return issue, nil
		}
	}

	return nil, s.Commit()
}

func gitlabIssueFromGitlabIssueRow(s xorm.Interface, issueRow gitlabIssueRow, testCaseID id.TestID) (*integration.GitLabIssue, error) {
	protocolRows, err := getCaseProtocolRows(s, testCaseID)
	if err != nil {
		return nil, err
	}

	for _, protocolRow := range protocolRows {
		if protocolRow.Id == issueRow.ProtocolId {
			protocol, err := buildCaseProtocol(s, protocolRow)
			if err != nil {
				return nil, err
			}
			issue := integration.NewIssue(&protocol, issueRow.GitlabId)
			return &issue, nil
		}
	}

	return nil, nil
}
