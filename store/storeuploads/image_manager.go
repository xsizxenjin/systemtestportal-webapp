// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package storeuploads

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

//"Enum" for Image Types
var ImageType = newImageTypeRegistry()

func newImageTypeRegistry() *imageTypeRegistry {
	return &imageTypeRegistry{
		Profile: "user",
		Project: "project",
	}
}

type imageTypeRegistry struct {
	Profile string
	Project string
}

// WriteProfilePictureToFile will write the profile image to file.
// The imageSource can either be a base64 encoded string or a relative path to an image.
// Only if the image is a base64 encoded string (which means the user uploaded a new image)
// then it will be written in the data/<username> directory for the project.
// If the image is a path, the user.Image property will be returned
func WriteProfilePictureToFile(imageSource string, user user.User) (string, error) {
	// if image is base64 encoded -> new image
	if strings.HasPrefix(imageSource, "data:image") {

		imageFolder := path.Join(DirUsers, escape(user.Name), DirUserImage)
		imagePath, err := writeImageToFile(imageSource, imageFolder, FileProfilePicture)

		if err != nil {
			return "", err
		}
		return imagePath, nil

	} else {
		// Image source is a reference to an image which is already present
		return user.Image, nil
	}
}

// WriteProjectImageToFile will write the project image to file.
// The imageSource can either be a base64 encoded string or a relative path to an image.
// Only if the image is a base64 encoded string (which means the user uploaded a new image)
// then it will be written in the data/<project name> directory for the project.
// If the image is a path, the project.Image property will be returned
func WriteProjectImageToFile(imageSource string, project project.Project) (string, error) {
	// if image is base64 encoded -> new image
	if strings.HasPrefix(imageSource, "data:image") {

		imageFolder := path.Join(DirProjects, escape(project.Name), DirProjectImage)
		imagePath, err := writeImageToFile(imageSource, imageFolder, FileProjectProfile)

		if err != nil {
			return "", err
		}
		return imagePath, nil

	} else {
		// Image source is a reference to an image which is already present
		return project.Image, nil
	}
}

// GetImagePath will check if the stored image is present in the data directory.
// If the image can not be found the path of the placeholder will be returned.
func GetImagePath(pathInDataBase string, imageType string) string {
	imagePath := path.Join(config.Get().DataDir, pathInDataBase)

	if _, err := os.Stat(imagePath); os.IsNotExist(err) || pathInDataBase == "" {
		// If image not present
		if imageType == ImageType.Profile {
			return PathProfilePlaceholderImage
		} else {
			return PathGenericPlaceholderImage
		}
	} else {
		// Return existing imagePath
		return imagePath
	}
}

// escape will replace all forbidden characters in the given string value
func escape(str string) (escaped string) {
	escaped = strings.Replace(str, ".", "-", -1)
	return
}

// transformBase64ToFileContentAndExtension returns
// (1) the base64 string content and
// (2) the file extension
// of a base64 encoded string
func transformBase64ToFileContentAndExtension(base64Str string) (string, string) {

	split := strings.Split(base64Str, ",")
	base64Meta := split[0]
	imgContent := split[1]
	imgExtension := strings.TrimSuffix(base64Meta[11:], ";base64")

	// Handle svg images; should be outsourced in a separate method if more formats needs to be escaped
	if imgExtension == "svg+xml" {
		imgExtension = "svg"
	}

	return imgContent, imgExtension
}

// writeImageToFile will write a base64Str to file and return the relative image path
// imageFolder: Relative/Absolute folder path; will be created if it not existing
// imageName: A string describing the name of the image
// Example: imageFolder = "/home/" ; imageName = "golang" ; base64Str = "data:image/png ..."
// Example-Output: The image will be written as: "/home/golang.png"
// The extension will be extracted from the base64Str
func writeImageToFile(base64Str string, imageFolder string, imageName string) (string, error) {

	imageFolderAbsolute := path.Join(config.Get().DataDir, imageFolder)

	err := os.MkdirAll(imageFolderAbsolute, os.ModePerm)
	if err != nil {
		return "", err
	}

	imgContent, imageExtension := transformBase64ToFileContentAndExtension(base64Str)

	imageBytes, err := base64.StdEncoding.DecodeString(imgContent)
	if err != nil {
		return "", err
	}

	imagePath := path.Join(imageFolderAbsolute, imageName+"."+imageExtension)
	err = ioutil.WriteFile(imagePath, imageBytes, os.ModePerm)
	if err != nil {
		return "", err
	}

	return strings.Replace(imagePath, config.Get().DataDir, "", 1), nil
}

// DeleteImage deletes an image from a given database path
func DeleteImage(path string, imageType string) error {
	if path == PathProfilePlaceholderImage || path == PathGenericPlaceholderImage {
		return nil
	}
	err := os.Remove(path)
	if err != nil {
		fmt.Println(err.Error())
	}
	return nil
}
