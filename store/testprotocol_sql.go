/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

//---------------------------------------------------------------------------------------------------------------------|
// TODO: Insert File Description Here
//---------------------------------------------------------------------------------------------------------------------|

package store

import (
	"github.com/go-xorm/xorm"
	"github.com/pkg/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity/activityItems"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"net/http"
)

type TestProtocolSQL struct {
	engine *xorm.Engine
}

//---------------------------------------------------------------------------------------------------------------------|
// FUNCTIONS
//---------------------------------------------------------------------------------------------------------------------|

func (testProtocolSQL TestProtocolSQL) Create(session *xorm.Session, activityType int, activityEntities *activity.ActivityEntities, request *http.Request) (int64, error) {

	newTestProtocol := &activityItems.TestProtocol{}

	switch activityType {
	case activity.PROTOCOL_CASE:
		newTestProtocol.TestProtocolId = activityEntities.CaseProtocol.Id
		newTestProtocol.TestId = activityEntities.Case.Id
		newTestProtocol.IsCase = true
	case activity.PROTOCOL_SEQUENCE:
		newTestProtocol.TestProtocolId = activityEntities.SequenceProtocol.Id
		newTestProtocol.TestId = activityEntities.Sequence.Id
		newTestProtocol.IsCase = false
	default:
		return 0, errors.Errorf("%s", "Could not find the specified test-protocol-activity type")
	}

	_, err := session.Insert(newTestProtocol)
	if err != nil {
		return 0, err
	}

	return newTestProtocol.Id , nil
}

func (testProtocolSQL TestProtocolSQL) GetLazyLoadedActivityItem(activity *activity.Activity) (activity.ActivityItem, error) {
	protocolStore := GetProtocolStore()
	testProtocol := activityItems.TestProtocol{}

	// First load the testprotocol
	has, err := testProtocolSQL.engine.Where("id = ?", activity.ActivityItemId).Get(&testProtocol)
	if err != nil {
		return nil, err
	} else if !has {
		err = errors.Errorf("%s%v%s", "The Activity with ID: ", activity.ActivityItemId, " has no activity-item")
		return nil, err
	}

	// Load the referenced protocol ----------------------------------------|
	if testProtocol.IsCase {
		caseExecutionProtocol, err := protocolStore.GetPartiallyLoadedCaseExecutionProtocol(testProtocol.TestProtocolId)
		if err != nil {
			return nil, err
		}

		testProtocol.CaseExecutionProtocol = caseExecutionProtocol
	} else {
		sequenceExecutionProtocol, err := protocolStore.GetPartiallyLoadedSequenceExecutionProtocol(testProtocol.TestProtocolId)
		if err != nil {
			return nil , err
		}

		testProtocol.SequenceExecutionProtocol = sequenceExecutionProtocol
	}
	// ---------------------------------------------------------------------------------------------|
	// Load the referenced test --------------------------------------------------------------------|
	if testProtocol.IsCase {
		testCase := &test.Case{}

		if testProtocol.CaseExecutionProtocol == nil {
			testCase = nil
		} else {
			has, err := testProtocolSQL.engine.Table("test_cases").Where("id = ?", testProtocol.TestId).Get(testCase)
			if err != nil {
				return nil, err
			} else if !has {
				testCase = nil
			}
		}

		testProtocol.TestCase = testCase
	} else {
		testSequence := &test.Sequence{}

		if testProtocol.SequenceExecutionProtocol== nil {
			testSequence = nil
		} else {
			has, err := testProtocolSQL.engine.Table("test_sequences").Where("id = ?", testProtocol.TestId).Get(testSequence)
			if err != nil {
				return nil, err
			} else if !has {
				testSequence = nil
			}
		}
		testProtocol.TestSequence = testSequence
	}
	// ---------------------------------------------------------------------------------------------|

	return testProtocol, nil
}