// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package store

import (
	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

const (
	caseVersionField    = "test_case_version"
	projectVariantField = "project_variant"
)

type caseVersionSUTVariantRow struct {
	ID              int64
	TestCaseVersion int64
	ProjectVariant  int64
}

func saveSUTVariantsForCaseVersion(s xorm.Interface, cvrID, svrID int64, nvs []project.Variant) error {
	var names []string
	for _, nv := range nvs {
		names = append(names, nv.Name)
	}

	svIDs, err := lookupSUTVariantRowIDs(s, svrID, names...)
	if err != nil {
		return err
	}

	var cvsvrs []caseVersionSUTVariantRow
	for _, svID := range svIDs {
		cvsvrs = append(cvsvrs, caseVersionSUTVariantRow{TestCaseVersion: cvrID, ProjectVariant: svID})
	}

	return insertCaseVersionSUTVariantRows(s, cvsvrs...)
}

func insertCaseVersionSUTVariantRows(s xorm.Interface, cvsvrs ...caseVersionSUTVariantRow) error {
	_, err := s.Table(caseVersionSUTVariantTable).Insert(&cvsvrs)
	return err
}

func listSUTVariantRowsForCaseVersion(s xorm.Interface, vrID int64) ([]sutVariantRow, error) {
	var ids []int64
	err := s.Table(caseVersionSUTVariantTable).Distinct(projectVariantField).In(caseVersionField, vrID).Find(&ids)
	if err != nil {
		return nil, err
	}

	var svrs []sutVariantRow
	err = s.Table(sutVariantTable).In(idField, ids).Asc(nameField).Find(&svrs)
	if err != nil {
		return nil, err
	}

	return svrs, nil
}
