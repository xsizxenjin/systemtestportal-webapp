/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package store

import (
	"fmt"
	"time"

	"github.com/go-xorm/xorm"
	"github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/task"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

const (
	errNoSuchReferenceType = "no reference type with the id %v"
)

type TaskSQL struct {
	e *xorm.Engine
}

type taskListRow struct {
	ID     int64
	UserID int64
}

type taskItemRow struct {
	ID            int64 // ID of the row
	TodoItemIndex int   // The index of the item in the task-list
	ListID        int64
	AssigneeID    int64
	AuthorID      int64
	ProjectID     int64
	Type          int
	ReferenceID   int64 // ID to the row of the reference
	ReferenceType int   // Type of the reference
	VersionID     int64
	VariantID     int64
	Deadline      time.Time
	CreationDate  time.Time
	Done          bool
}

func (taskSQL TaskSQL) Add(list task.List) error {
	session := taskSQL.e.NewSession()
	defer session.Close()

	if err := session.Begin(); err != nil {
		return err
	}

	err := saveTaskList(session, list)
	if err != nil {
		err = multierror.Append(err, session.Rollback())
		return err
	}

	return session.Commit()
}

// saveTaskList saves a task-list and every task-item
func saveTaskList(s xorm.Interface, list task.List) error {
	userID, err := lookupUserRowIDForUser(s, id.NewActorID(list.Username))
	if err != nil {
		return err
	}

	listRow := taskListRow{
		UserID: userID,
	}

	listID, ex, err := getTaskListRowID(s, userID)
	if err != nil {
		return err
	}
	if !ex {
		err = insertTaskListRow(s, &listRow)
		if err != nil {
			return err
		}
	} else {
		listRow.ID = listID
	}

	return saveTaskItems(s, list, listRow.ID)
}

// getTaskListRowID returns the ID of the list for the given owner
func getTaskListRowID(s xorm.Interface, userID int64) (int64, bool, error) {
	list, ex, err := getTaskListRow(s, userID)
	return list.ID, ex, err
}

// getTaskListRow returns the row of the list for the given user
func getTaskListRow(s xorm.Interface, userID int64) (taskListRow, bool, error) {
	list := taskListRow{
		UserID: userID,
	}
	ex, err := s.Table(taskListTable).Get(&list)
	if err != nil || !ex {
		return taskListRow{}, false, err
	}

	return list, true, nil
}

// insertTaskListRow inserts a new task-list into the table
func insertTaskListRow(s xorm.Interface, listRow *taskListRow) error {
	_, err := s.Table(taskListTable).Insert(listRow)
	return err
}

// saveTaskItems saves all task-items of the given task-list
func saveTaskItems(s xorm.Interface, list task.List, listID int64) error {
	for _, item := range list.Tasks {
		err := saveTaskItem(s, item, listID)
		if err != nil {
			return err
		}
	}

	return nil
}

// saveTaskItem saves (inserts or updates) the given task-item
func saveTaskItem(s xorm.Interface, item task.Item, listID int64) error {
	// Check if item exists, if exists -> update, else insert
	itemRow, ex, err := getTaskItemRow(s, item.Index, listID)
	if err != nil {
		return err
	}

	if !ex {
		itemRow, err = fillItemRow(s, itemRow, item)
		if err != nil {
			return err
		}
		err = insertTaskItemRow(s, itemRow)
	} else {
		// The only value that can change after creation
		// is the "DONE" status
		itemRow.Done = item.Done

		err = updateTaskItemRow(s, itemRow)
	}

	return err
}

// fillItemRow fills the itemRow with the information of the given item.
//
// Returns the filled itemRow or an error if any occurred.
func fillItemRow(s xorm.Interface, itemRow *taskItemRow, item task.Item) (*taskItemRow, error) {

	// Add assigneeID to itemRow
	assigneeRow, ex, err := getUserRow(s, item.Assignee)
	if !ex || err != nil {
		return nil, err
	}
	itemRow.AssigneeID = assigneeRow.ID

	// Add authorID to itemRow
	authorRow, ex, err := getUserRow(s, item.Author)
	if !ex || err != nil {
		return nil, err
	}
	itemRow.AuthorID = authorRow.ID

	// Add projectID to itemRow
	projectRowID, ex, err := getProjectRowID(s, item.ProjectID)
	if !ex || err != nil {
		return nil, err
	}
	itemRow.ProjectID = projectRowID

	// Set the type of the item
	itemRow.Type = int(item.Type)

	// Set id to the reference
	switch item.Reference.Type {
	case task.Case:
		caseID, ex, err := getCaseRowID(s, id.NewTestID(item.ProjectID, item.Reference.ID, true))
		if !ex || err != nil {
			return nil, err
		}
		itemRow.ReferenceID = caseID
		itemRow.ReferenceType = int(task.Case)
	case task.Sequence:
		seqID, ex, err := getSequenceRowID(s, id.NewTestID(item.ProjectID, item.Reference.ID, false))
		if !ex || err != nil {
			return nil, err
		}
		itemRow.ReferenceID = seqID
		itemRow.ReferenceType = int(task.Sequence)
	case task.SUTVersion:
		verID, err := lookupSUTVersionRowID(s, projectRowID, item.Reference.ID)
		if err != nil {
			return nil, err
		}
		itemRow.ReferenceID = verID
		itemRow.ReferenceType = int(task.SUTVersion)
	default:
		return nil, fmt.Errorf(errNoSuchReferenceType, itemRow.ReferenceType)
	}

	versionID, err := lookupSUTVersionRowID(s, projectRowID, item.Version)
	if err != nil {
		return nil, err
	}
	itemRow.VersionID = versionID

	variantIDs, err := lookupSUTVariantRowIDs(s, versionID, item.Variant)
	if err != nil {
		return nil, err
	}
	itemRow.VariantID = variantIDs[0]

	itemRow.Deadline = item.Deadline
	itemRow.CreationDate = item.CreationDate
	itemRow.Done = item.Done

	return itemRow, nil
}

// getTaskItemRow returns the row of the task-item with the given itemID
// from the list with the given listID.
//
// If an error occurred, false and the error
// will be returned. If the item does not exist, the row with the index
// and the listID will be returned.
func getTaskItemRow(s xorm.Interface, itemID int, listID int64) (*taskItemRow, bool, error) {
	itemRow := taskItemRow{
		TodoItemIndex: itemID,
		ListID:        listID,
	}
	ex, err := s.Table(taskItemTable).Get(&itemRow)
	if err != nil {
		return &taskItemRow{}, false, err
	}
	if !ex {
		return &itemRow, false, nil
	}
	return &itemRow, true, nil
}

func insertTaskItemRow(s xorm.Interface, itemRow *taskItemRow) error {
	_, err := s.Table(taskItemTable).Insert(itemRow)
	return err
}

func updateTaskItemRow(s xorm.Interface, itemRow *taskItemRow) error {
	// AllCols() is needed because boolean columns are not updated by default
	affected, err := s.Table(taskItemTable).ID(itemRow.ID).AllCols().Update(itemRow)
	if err != nil {
		return err
	}

	if affected != 1 {
		return fmt.Errorf(errorNoAffectedRows, affected)
	}

	return nil
}

func (taskSQL TaskSQL) Get(username string) (*task.List, error) {
	s := taskSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}
	// Get id of user
	userRow, ex, err := getUserRow(s, id.NewActorID(username))
	if !ex || err != nil {
		return nil, err
	}
	// Get list for user
	listRow, ex, err := getTaskListRow(s, userRow.ID)
	if err != nil {
		return nil, err
	}
	var taskList *task.List
	// If the user does not have a list, create a new one,
	// because every user has to have one!
	if !ex {
		taskList = task.NewList(userRow.Name)
	} else {
		taskItems, err := getTaskItems(s, listRow.ID)
		if err != nil {
			return nil, err
		}
		taskList = task.NewList(userRow.Name)
		for _, item := range taskItems {
			taskList.Tasks = append(taskList.Tasks, *item)
		}
	}

	return taskList, s.Commit()
}

// getTaskItems returns all task-items for the list with the given row-id
func getTaskItems(s xorm.Interface, listRowID int64) ([]*task.Item, error) {
	itemRows, err := listItemRows(s, listRowID)
	if err != nil {
		return nil, err
	}
	var taskItems []*task.Item
	for _, itemRow := range itemRows {
		item, err := fillTaskItem(s, itemRow)
		if err != nil {
			return nil, err
		}

		taskItems = append(taskItems, item)
	}

	return taskItems, nil
}

// fillTaskItem inserts ALL information into an item
func fillTaskItem(s xorm.Interface, itemRow taskItemRow) (*task.Item, error) {
	item := taskFromItemRow(itemRow)
	// Add projectID to task-item
	projectRow, ex, err := getProjectRowByID(s, itemRow.ProjectID)
	if !ex || err != nil {
		return nil, err
	}
	ownerRow, err := getOwnerRowByID(s, projectRow.OwnerID)
	if err != nil {
		return nil, err
	}
	item.ProjectID = id.NewProjectID(id.NewActorID(ownerRow.Actor()), projectRow.Name)
	project, ok, err := GetProjectStore().Get(item.ProjectID)
	if err != nil || !ok {
		return nil, err
	}
	item.ProjectImage = project.Image

	// Add assignee to task
	assigneeRow, ex, err := getUserRowByID(s, itemRow.AssigneeID)
	if !ex || err != nil {
		return nil, err
	}
	item.Assignee = id.NewActorID(assigneeRow.Name)

	// Add author to task-item
	userRow, ex, err := getUserRowByID(s, itemRow.AuthorID)
	if !ex || err != nil {
		return nil, err
	}
	item.Author = id.NewActorID(userRow.Name)
	// Depending on itemRow.ReferenceType, select the reference item from the respective table
	switch itemRow.ReferenceType {
	case int(task.Case):
		item.Reference.Type = task.Case
		caseRow, ex, err := getCaseRowByID(s, itemRow.ReferenceID)
		if !ex || err != nil {
			return nil, err
		}
		item.Reference.ID = caseRow.Name
	case int(task.Sequence):
		item.Reference.Type = task.Sequence
		sequenceRow, ex, err := getSequenceRowByID(s, itemRow.ReferenceID)
		if !ex || err != nil {
			return nil, err
		}
		item.Reference.ID = sequenceRow.Name
	case int(task.SUTVersion):
		item.Reference.Type = task.SUTVersion
		versionRow, ex, err := getSUTVersionRowByID(s, itemRow.ReferenceID)
		if !ex || err != nil {
			return nil, err
		}
		item.Reference.ID = versionRow.Name
	default:
		return nil, fmt.Errorf(errNoSuchReferenceType, itemRow.ReferenceType)
	}
	// Add version and variant to task.
	// It is not important whether the row actually exists. If it
	// does not exists, the version+variant is just an empty string.
	versionRow, _, err := getSUTVersionRowByID(s, itemRow.VersionID)
	if err != nil {
		return nil, err
	}
	item.Version = versionRow.Name

	variantRow, _, err := getSUTVariantRowByID(s, itemRow.VariantID)
	if err != nil {
		return nil, err
	}
	item.Variant = variantRow.Name

	return item, nil
}

// listItemRows returns all taskItemRows for the list with the given row-id
func listItemRows(s xorm.Interface, listRowID int64) ([]taskItemRow, error) {
	var itemRows []taskItemRow
	err := s.Table(taskItemTable).Desc(taskItemIndexField).Find(&itemRows, &taskItemRow{ListID: listRowID})
	if err != nil {
		return nil, err
	}
	return itemRows, nil
}

func getTaskListRowByID(s xorm.Interface, listRowID int64) (taskListRow, error) {
	var row taskListRow
	_, err := s.Table(taskListTable).ID(listRowID).Get(&row)
	return row, err
}

// taskFromItemRow converts a taskItemRow to its task-item with the
// information it contains. The references (to project, author, reference)
// are not resolved. These attributes have to be added separately.
func taskFromItemRow(row taskItemRow) *task.Item {
	return &task.Item{
		Index:        row.TodoItemIndex,
		Type:         task.Type(row.Type),
		Deadline:     row.Deadline,
		CreationDate: row.CreationDate,
		Done:         row.Done,
	}
}

func deleteTasks(s xorm.Interface, refType int, rowId int64) error {
	_, err := s.Table(taskItemTable).
		Where("reference_type = ?", refType).
		Where("reference_id = ?", rowId).
		Delete(&taskItemRow{})
	return err
}

// GetTasksForTest retrieves the users from open tasks for a test, identified by the testID
func (taskSQL TaskSQL) GetTasksForTest(testID id.TestID) ([]*task.Item, error) {
	s := taskSQL.e.NewSession()
	defer s.Close()

	refType := int(task.Case)
	var rowID int64

	// get referenceID depending on refType
	if !testID.IsCase() {
		refType = int(task.Sequence)
		rID, ex, err := getSequenceRowID(s, testID)
		if err != nil || !ex {
			return []*task.Item{}, err
		}
		rowID = rID
	} else {
		rID, ex, err := getCaseRowID(s, testID)
		if err != nil || !ex {
			return []*task.Item{}, err
		}
		rowID = rID
	}

	// get related tasks
	var taskRows []taskItemRow
	err := s.Table(taskItemTable).
		Where("reference_type = ?", refType).
		Where("reference_id = ?", rowID).
		Where("done = ?", false).
		Find(&taskRows)
	if err != nil {
		return []*task.Item{}, err
	}

	var taskItems []*task.Item
	for _, taskRow := range taskRows {
		taskItem, err := fillTaskItem(s, taskRow)
		if err != nil {
			return nil, err
		}
		taskItems = append(taskItems, taskItem)
	}

	return taskItems, s.Commit()
}

// AddItem adds the given item to the store
func (taskSQL TaskSQL) AddItem(item task.Item) error {
	s := taskSQL.e.NewSession()
	defer s.Close()

	userRow, ex, err := getUserRow(s, item.Assignee)
	if !ex || err != nil {
		return err
	}

	taskListRow, ex, err := getTaskListRow(s, userRow.ID)
	if !ex || err != nil {
		return err
	}

	err = saveTaskItem(s, item, taskListRow.ID)
	if err != nil {
		return err
	}

	return s.Commit()
}

func deleteTaskListForUser(s xorm.Interface, u *user.User) error {
	ur, ex, err := getUserRow(s, u.ID())
	if err != nil {
		return err
	}
	if !ex {
		return fmt.Errorf("User not found")
	}

	_, err = s.Table("todo_lists").Delete(&taskListRow{UserID: ur.ID})
	return err
}
