/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package store

import (
	"github.com/go-xorm/xorm"
	"github.com/pkg/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

type CommentsSQL struct {
	engine *xorm.Engine
}

/*
LazyLoad will load associations into the comment. Currently loaded are the user and the role
*/
func (commentsSql CommentsSQL) lazyLoad(comment *comment.Comment, project *project.Project) error {

	var commentAuthor user.User

	_, err := commentsSql.engine.Table("users").Where("name = ?", comment.AuthorId).Get(&commentAuthor)
	if err != nil {
		return err
	}

	comment.AuthorRole = project.GetRole(&commentAuthor)
	comment.Author = &commentAuthor

	return nil
}

func (commentsSql CommentsSQL) GetComment(commentId int64, project *project.Project) (*comment.Comment, error) {
	var comment = comment.Comment{Id: commentId}

	_, err := commentsSql.engine.Get(&comment)
	if err != nil {
		return nil, err
	}

	err = commentsSql.lazyLoad(&comment, project)

	return &comment, nil
}

/*
Generates foreign keys for a comment and inserts it into the db. CAUTION: YOU MAY NOT PASS POINTERS TO CASES/SEQUENCES.
DEREFERENCE THEM WITH *
*/
func (commentsSql CommentsSQL) InsertComment(comment *comment.Comment, testObject interface{}, user *user.User) error {
	err := setCommentForeignKeys(comment, testObject, user)
	if err != nil {
		return err
	}

	_, err = commentsSql.engine.Insert(comment)
	if err != nil {
		return err
	}

	return err
}

func setCommentForeignKeys(comment *comment.Comment, testObject interface{}, user *user.User) error {
	// Set the testID/versionNr based on if its a case or a sequence
	if testCase, ok := testObject.(test.Case); ok {
		comment.TestId = testCase.Id
		comment.TestVersionNr = testCase.TestCaseVersions[0].VersionNr
		comment.IsCase = true
	} else if testSequence, ok := testObject.(test.Sequence); ok {
		comment.TestId = testSequence.Id
		comment.TestVersionNr = testSequence.SequenceVersions[0].VersionNr
		comment.IsCase = false
	} else {
		return errors.New("Error while casting test to case or sequence for Id generation")
	}

	// Author Id ----------------------------|
	comment.AuthorId = user.Name

	return nil
}

/*
Gets comments for a testObject and fills them with users. CAUTION: YOU MAY NOT PASS POINTERS TO CASES/SEQUENCES.
DEREFERENCE THEM WITH *
*/
func (commentsSql CommentsSQL) GetCommentsForTest(testObject interface{}, project *project.Project, requester *user.User) ([]*comment.Comment, error) {
	var testId int64
	var comments []*comment.Comment
	var isCase bool

	// Build the testID based on if its a case or a sequence or a testId object
	if testCase, ok := testObject.(test.Case); ok {
		testId = testCase.Id
		isCase = true
	} else if testSequence, ok := testObject.(test.Sequence); ok {
		testId = testSequence.Id
		isCase = false
	} else {
		return nil, errors.New("Error while casting test to case or sequence or testId for Id generation")
	}

	// Query the user table
	err := commentsSql.engine.Where("test_id = ?", testId).And("is_case = ?", isCase).Find(&comments)
	if err != nil {
		return nil, err
	}

	for _, comment := range comments {
		err = commentsSql.lazyLoad(comment, project)
		if err != nil {
			return nil, err
		}

		comment.Requester = requester
	}
	return comments, nil
}

func (commentsSql CommentsSQL) DeleteCommentsForTest(testObject interface{}) error {

	// Call GetCommentsForTest with project = nil so that roles arent lazy loaded
	comments, err := commentsSql.GetCommentsForTest(testObject, nil, nil)
	if err != nil {
		return err
	}

	for _, comment := range comments {

		_, err := commentsSql.engine.Where("test_id = ?", comment.TestId).Delete(comment)
		if err != nil {
			return err
		}

		_, err = commentsSql.engine.Unscoped().Where("test_id = ?", comment.TestId).Delete(comment)
		if err != nil {
			return err
		}
	}
	return nil
}

func (commentsSql CommentsSQL) UpdateComment(comment *comment.Comment) error {
	_, err := commentsSql.engine.Id(comment.Id).UseBool().Update(comment)
	if err != nil {
		return err
	}

	return nil
}

func removeUserFromComment(s xorm.Interface, u *user.User) error {
	count, err := s.Table("comment").Count(&comment.Comment{AuthorId: u.Name})
	if err != nil {
		return err
	}

	if count > 0 {
		_, err = s.Table("comment").Update(&comment.Comment{AuthorId: ""}, &comment.Comment{AuthorId: u.Name})
		return err
	}
	return nil
}
