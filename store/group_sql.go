// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package store

import (
	"fmt"
	"time"

	"github.com/go-xorm/xorm"
	multierror "github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

type GroupSQL struct {
	e *xorm.Engine
}

type groupRow struct {
	ID           int64
	Name         string
	Description  string
	Visibility   int
	CreationDate time.Time
}

func groupRowFromGroup(g *group.Group) groupRow {
	return groupRow{
		Name:         g.Name,
		Description:  g.Description,
		Visibility:   int(g.Visibility),
		CreationDate: g.CreationDate,
	}
}

func groupFromRow(r groupRow) *group.Group {
	return &group.Group{
		Name:         r.Name,
		Description:  r.Description,
		Visibility:   visibility.Visibility(r.Visibility),
		CreationDate: r.CreationDate,
	}
}

func (s GroupSQL) List() ([]*group.Group, error) {
	return listGroups(s.e)
}

func (s GroupSQL) Add(g *group.Group) error {
	session := s.e.NewSession()
	defer session.Close()

	err := session.Begin()
	if err != nil {
		return err
	}

	err = saveGroup(session, g)
	if err != nil {
		err = multierror.Append(err, session.Rollback())
		return err
	}

	return session.Commit()
}

func (s GroupSQL) Get(id id.ActorID) (*group.Group, bool, error) {
	return getGroup(s.e, id)
}

func saveGroup(s xorm.Interface, g *group.Group) error {
	ogr := groupRow{Name: g.Name}
	ex, err := s.Table(groupTable).Get(&ogr)
	if err != nil {
		return err
	}

	ngr := groupRowFromGroup(g)
	if !ex {
		err = insertGroupRow(s, &ngr)
	} else {
		ngr.ID = ogr.ID
		err = updateGroupRow(s, &ngr)
	}

	return err
}

func updateGroupRow(s xorm.Interface, gr *groupRow) error {
	err := updateOwnerForGroup(s, gr)
	if err != nil {
		return err
	}

	aff, err := s.Table(groupTable).ID(gr.ID).Update(gr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func insertGroupRow(s xorm.Interface, gr *groupRow) error {
	err := insertOwnerForGroup(s, gr)
	if err != nil {
		return err
	}

	_, err = s.Table(groupTable).Insert(gr)

	return err
}

func listGroups(s xorm.Interface) ([]*group.Group, error) {
	grs, err := listGroupRows(s)
	if err != nil {
		return nil, err
	}

	var gs []*group.Group
	for _, gr := range grs {
		gs = append(gs, groupFromRow(gr))
	}

	return gs, nil
}

func getGroup(s xorm.Interface, id id.ActorID) (*group.Group, bool, error) {
	gr, ex, err := getGroupRow(s, id.Actor())
	if err != nil || !ex {
		return nil, false, err
	}

	return groupFromRow(gr), true, nil
}

func listGroupRows(s xorm.Interface) ([]groupRow, error) {
	var grs []groupRow
	err := s.Table(groupTable).Asc(nameField).Find(&grs)
	if err != nil {
		return nil, err
	}

	return grs, nil
}

func getGroupRow(s xorm.Interface, name string) (groupRow, bool, error) {
	gr := groupRow{Name: name}
	ex, err := s.Table(groupTable).Get(&gr)
	if err != nil || !ex {
		return groupRow{}, false, err
	}

	return gr, true, nil
}
