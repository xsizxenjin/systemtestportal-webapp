/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$.getScript("/static/js/util/common.js");
$.getScript("/static/js/util/ajax.js");
$.ajax("/static/js/util/common.js", {async:false, dataType: "script"});

var tabMapping = Object.freeze({
    dashboard: {name: "dashboard", url: "dashboard"},
    testcases: {name: "testcases", url: "testcases"},
    testsequences: {name: "testsequences", url: "testsequences"},
    protocols: {name: "protocols", url: "protocols"},
    members: {name: "members", url: "members"},
    settings: {name: "settings", url: "settings/project"},
    activity: {name: "activity", url: "activity"}
});

/** Add click listeners to tabs and project image */
function initializeTabClickListener() {

    // Dashboard
    $("#tabButtonDashboard, #menuButtonDashboard, #projectImage").on("click", event => requestTab(event, tabMapping.dashboard));

    // Test Cases
    $("#tabButtonTestCases, #menuButtonTestCases").on("click", event => requestTab(event, tabMapping.testcases));

    // Test Sequences
    $("#tabButtonTestSequences, #menuButtonTestSequences").on("click", event => requestTab(event, tabMapping.testsequences));

    // Test Protocols
    $("#tabButtonProtocols, #menuButtonProtocols").on("click", event => requestTab(event, tabMapping.protocols));

    // Members
    $("#tabButtonMembers, #menuButtonMembers").on("click", event => requestTab(event, tabMapping.members));

    // Settings
    $("#tabButtonSettings, #menuButtonSettings").on("click", event => requestTab(event, tabMapping.settings));

    // Activity
    $("#tabButtonActivity, #menuButtonActivity").on("click", event => requestTab(event, tabMapping.activity));
}

/**
 * Updates menu highlight on tab/menu swap
 * @param tab {string} menu entry to highlight
 */
function updateMenu(tab) {

    $(".tab-collapse-menu .dropdown-item.active").removeClass("active");

    let menu = null;
    if (tab === tabMapping.testcases.name)
        menu = $("#menuButtonTestCases");
    else if (tab === tabMapping.testsequences.name)
        menu = $("#menuButtonTestSequences");
    else if (tab === tabMapping.protocols.name)
        menu = $("#menuButtonProtocols");
    else if (tab === tabMapping.members.name)
        menu = $("#menuButtonMembers");
    else if (tab === tabMapping.dashboard.name)
        menu = $("#menuButtonDashboard");
    else if (tab === tabMapping.settings.name)
        menu = $("#menuButtonSettings");
    else if (tab === tabMapping.activity.name)
        menu = $("#menuButtonActivity");

    if (menu != null)
        menu.addClass("active");
}

/**
 * Updates tab highlight on tab/menu swap
 * @param tab {string} the tab to highlight
 */
function updateTabs(tab) {

    $(".nav-tabs .nav-link.active").removeClass("active");

    let newTab = null;
    if (tab === tabMapping.testcases.name)
        newTab = $("#tabButtonTestCases");
    else if (tab === tabMapping.testsequences.name)
        newTab = $("#tabButtonTestSequences");
    else if (tab === tabMapping.protocols.name)
        newTab = $("#tabButtonProtocols");
    else if (tab === tabMapping.members.name)
        newTab = $("#tabButtonMembers");
    else if (tab === tabMapping.dashboard.name)
        newTab = $("#tabButtonDashboard");
    else if (tab === tabMapping.settings.name)
        newTab = $("#tabButtonSettings");
    else if (tab === tabMapping.activity.name)
        newTab = $("#tabButtonActivity");
    if (newTab != null)
        newTab.addClass("active");
}

/** jQuery document.ready */
$(() => {

    let url = currentURL().segments[3];
    updateMenu(url);
    updateTabs(url);

    $("time.timeago").timeago();
});