/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Saves the system settings using a put request
 */
$("#saveButton").click(function() {
    const isAccessAllowed = $("#inputIsAccessAllowed").prop('checked');
    const isRegistrationAllowed = $("#inputIsRegistrationAllowed").prop('checked');
    const isEmailVerification = $("#inputIsEmailVerification").prop('checked');
    const isDeleteUsers = $("#inputIsDeleteUsers").prop('checked');
    const isExecutionTime = $("#inputIsExecutionTime").prop('checked');

    const isGlobalMessageExpirationDate = $("#inputIsGlobalMessageExpirationDate").prop('checked');
    const isDisplayMessage = $("#inputIsDisplayMessage").prop('checked');
    const globalMessage = $("#inputGlobalMessage").val();
    const globalMessageType = $('input[name=inputGlobalMessageTypeRadio]:checked').val();
    const globalMessageTimeStamp = Math.floor(Date.now() / 1000);
    const globalMessageExpirationDate = $("#datepicker").val().toString()
    const globalMessageExpirationDateUnix = Math.floor(Date.parse($("#datepicker").val()) / 1000);

    const imprintMessage = $("#inputImprintMessage").val();
    const privacyMessage = $("#inputPrivacyMessage").val();

    $.ajax({
        url: currentURL().takeFirstSegments(0) + "/settings",
        type: "PUT",
        data: {
            "isAccessAllowed": isAccessAllowed,
            "isRegistrationAllowed": isRegistrationAllowed,
            "isEmailVerification": isEmailVerification,
            "isDeleteUsers": isDeleteUsers,
            "isGlobalMessageExpirationDate": isGlobalMessageExpirationDate,
            "isDisplayMessage": isDisplayMessage,
            "isExecutionTime": isExecutionTime,
            "globalMessage": globalMessage,
            "globalMessageType": globalMessageType,
            "globalMessageTimeStamp": globalMessageTimeStamp,
            "globalMessageExpirationDate": globalMessageExpirationDate,
            "globalMessageExpirationDateUnix": globalMessageExpirationDateUnix,
            "imprintMessage": imprintMessage,
            "privacyMessage": privacyMessage
        },
        statusCode: {
            200: function() {
                location.reload(true);
            }
        }

    }).fail(response => {
        $("#errorModelResponseTextSystemSettings").empty().append(response.responseText);
        $("#errorModal").modal("show");
    });
});

$(() => {
    $("[data-toggle='tooltip']").tooltip();
})