/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$.getScript("/static/js/util/common.js");
$.getScript("/static/js/util/ajax.js");

$(".btn-task-item-done").on("click", function (event) {
    const btnValue = $(this).val();

    const listItem = $("#task-item-" + btnValue);
    const button = $("#btn-task-item-done-" + btnValue);

    if (button.hasClass("btn-primary")){
        listItem.css("opacity", 0.6);
        button.addClass("btn-secondary")
            .removeClass("btn-primary")
            .text(UNDONE)
            .attr("title", "Set the task to undone");
    } else {
        listItem.css("opacity", 1);
        button.addClass("btn-primary")
            .removeClass("btn-secondary")
            .text(DONE)
            .attr("title", "Set the task to done");
    }

    ajaxSendDataToServer(event, currentURL(), btnValue);
});

$(".btn-task-item-start").on("click", function (event) {
    window.location = this.value;
});

$(() => {

    $("time.timeago").timeago();

    $('[data-toggle="tooltip"]').tooltip({
        trigger : 'hover'
    });

});