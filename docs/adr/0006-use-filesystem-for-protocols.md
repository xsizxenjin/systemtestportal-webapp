# Use filesystem to store protocols

## Context and Problem Statement

The protocols for test executions need to be persistently stored. What possibilites are there to store them?

## Decision Drivers

* Portability (Easy to export)
* Edit them after saving

## Considered Options

* Filesystem
* Database

## Decision Outcome

Chosen option: "Filesystem", because a filesystem provides an easy way to export the protocols.
