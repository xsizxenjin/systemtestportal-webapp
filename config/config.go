/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package config

import (
	"path"

	"flag"

	"os"

	"io/ioutil"

	"log"

	"gitlab.com/silentteacup/congo"
	congoflag "gitlab.com/silentteacup/congo/sources/flag"
	"gitlab.com/silentteacup/congo/sources/ini"
	"gitlab.com/stp-team/systemtestportal-webapp/email"
)

// Configuration contains the configuration for the system.
// nolint
type Configuration struct {
	BasePath             string `name:"basepath" usage:"Defines the folder where STP searches its resources. \nIf this is not explicitly given STP will try the directory it's executed in."`
	Port                 int    `name:"port" usage:"Defines the port number STP will listen to."`
	Host                 string `name:"host" usage:"Defines the host STP will use.\nDefault is the empty string which means STP will listen to all interfaces."`
	DataDir              string `name:"data-dir" usage:"Where persistent data should be stored. This is were STP will store projects, testcases, users, ... and so on."`
	Debug                bool   `name:"debug" usage:"Enable this to get more detailed log output. Default is false."`
	SSLRedirect          bool   `name:"sslredirect" usage:"If SSLRedirect is set to true, then only allow HTTPS requests. Default is false."`
	SSLTemporaryRedirect bool   `name:"ssltemporaryredirect" usage:"If SSLTemporaryRedirect is true, the a 302 will be used while redirecting. Default is false (301)."`
	STSeconds            int64  `name:"stseconds" usage:"STSeconds is the max-age of the Strict-Transport-Security header. Default is 0, which would NOT include the header."`
	STSPreload           bool   `name:"stspreload" usage:"If STSPreload is set to true, the preload flag will be appended to the Strict-Transport-Security header. Default is false."`
	ForceSTSHeader       bool   `name:"forcestsheader" usage:"STS header is only included when the connection is HTTPS. If you want to force it to always be added, set to true. Default is false."`
	FrameDeny            bool   `name:"framedeny" usage:"If FrameDeny is set to true, adds the X-Frame-Options header with the value of 'DENY'. Default is false."`
	ContentTypeNosniff   bool   `name:"contenttypenosniff" usage:"If ContentTypeNosniff is true, adds the X-Content-Type-Options header with the value 'nosniff'. Default is false."`
	BrowserXssFilter     bool   `name:"browserxssfilter" usage:"If BrowserXssFilter is true, adds the X-XSS-Protection header with the value '1;mode=block'. Default is false."`
	ReferrerPolicy       string `name:"referrerpolicy" usage:"ReferrerPolicy allows the Referrer-Policy header with the value to b set with a custom value. Default is empty."`
	FeaturePolicy        string `name:"featurepolicy" usage:"FeaturePolicy allows the Feature-Policy header with the value to be set with a custom value. Default is empty."`

	CSP string `name:"csp" usage:"CSP header. Default is empty."`
}

// nonWindowsDefaultPath is the path used to search for the configuration file on
// non windows machines.
var nonWindowsDefaultPath = path.Join("/", "etc", "stp", "config.ini")
var configuration = getDefaults()

// Load loads the configuration or returns an error if something went wrong.
func Load() error {
	iniSource := getIniSource()
	cfg := congo.New(
		"main",
		congoflag.FromFlagSet(flag.NewFlagSet("empty", flag.ExitOnError), standardLoader),
		iniSource,
	)
	// Load config of application
	cfg.Using(&configuration)
	// Load mail-server config
	mailConfig := email.GetConfiguration()
	cfg.Using(&mailConfig)
	// Load authentication for mail-server
	emailAuth := email.GetAuthentication()
	cfg.Using(&emailAuth)

	if err := cfg.Init(); err != nil {
		return err
	}
	if err := cfg.Load(); err != nil {
		return err
	}

	// Set the loaded config
	initBasePath(Get().BasePath)
	email.SetConfiguration(mailConfig)
	email.SetAuthentication(emailAuth)

	return nil
}

// getDefaults returns the Configuration struct containing all defaults.
func getDefaults() Configuration {
	return Configuration{
		BasePath:             getWorkingDir(),
		Port:                 8080,
		Host:                 "",
		DataDir:              defaultDataDir,
		Debug:                false,
		SSLRedirect:          false,
		SSLTemporaryRedirect: false,
		STSeconds:            0,
		STSPreload:           false,
		ForceSTSHeader:       false,
		FrameDeny:            false,
		ContentTypeNosniff:   false,
		BrowserXssFilter:     false,
		ReferrerPolicy:       "",
		FeaturePolicy:        "",
		CSP:                  "",
	}
}

// getIniSource gets the source that loads from the ini configuration file
func getIniSource() ini.Source {
	sourcePath := defaultPath
	// Use different flag set that continues on error so we can parse the
	// setting folder flag before parsing the settings.
	s := flag.NewFlagSet("pre-flags", flag.ContinueOnError)

	name := "config-path"
	usage := "Can be " +
		"used to set the source were STP loads it's configuration from.\n Default is os depended. " +
		"On windows it's the working directory (.\\config.ini). On all other platforms it's " +
		nonWindowsDefaultPath + "."

	// Hide output of the first parsing. The second one will do it anyway.
	s.SetOutput(ioutil.Discard)
	s.StringVar(&sourcePath, name, sourcePath, usage)
	// Still add the flag to our main flag set so it appears in the help.
	flag.StringVar(&sourcePath, name, sourcePath, usage)
	// Ignore error since it will be picked up by second flagset
	s.Parse(os.Args[1:]) // nolint: errcheck

	if _, err := os.Stat(sourcePath); os.IsNotExist(err) && sourcePath != defaultPath {
		log.Printf("The configuration file specified by the %s argument "+
			"doesn't exist. STP will use the defaults instead.", name)
	}

	return ini.FromFile(sourcePath)
}

// Get returns a copy of the configuration.
func Get() Configuration {
	return configuration
}

// standardLoader loads the commandline arguments
func standardLoader() []string {
	return os.Args[1:]
}
