#!/bin/bash
#
# This file is part of SystemTestPortal.
# Copyright (C) 2018  Institute of Software Technology, University of Stuttgart
#
# SystemTestPortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SystemTestPortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
#

# import gpg keys
echo "$STP_GPG_PUBLIC" > stp-public.key
gpg --import stp-public.key
echo "$STP_GPG_PRIVATE" > stp-private.key
gpg --allow-secret-key-import --import stp-private.key

# output directory of the compile jobs: this directory has to be located in the /builds folder for
# gitlab to be able to save it as artifacts
FILES=/builds/stp-team/systemtestportal-webapp/binaries/*
BINARYDIR=/builds/stp-team/systemtestportal-webapp/binaries

ARCHDIR=/builds/stp-team/systemtestportal-webapp/archive
ROOTDIR=/root/go/src/gitlab.com/stp-team/systemtestportal-webapp

# Move to archive directory to prevent directory structured being copied into archive
mkdir "$ARCHDIR"
cd $ARCHDIR

# iterate over all binaries, sign them and package them
for file in $FILES; do
  echo $PWD
  echo "FILE: "$file
  # determine correct packaging method
  if [[ $file == *"windows"* ]]; then 
    pack_cmd="zip -r"
    file_name=$file".zip"
  else 
    pack_cmd="tar -cvzf"
    file_name=$file".tar.gz"
  fi
  # get file name without previous directory structure
  dir=$(basename $file)

  echo "FILE_NAME"$dir
  echo "ARCHIVE_NAME"$file_name


  # Create directory that will be archived
  mkdir "$dir" 

  # copy files to be extracted to archive directory
  mv $file $ARCHDIR/$dir/
  cp -R $ROOTDIR/templates $ARCHDIR/$dir/
  cp -R $ROOTDIR/static $ARCHDIR/$dir/
  cp -R $ROOTDIR/migrations $ARCHDIR/$dir/
  cp -R $ROOTDIR/data $ARCHDIR/$dir/
  cp $ROOTDIR/README.md $ARCHDIR/$dir/
  cp $ROOTDIR/LICENSE $ARCHDIR/$dir/

  $pack_cmd $file_name ./$dir

  rm -R "$dir"
done

cd "$BINARYDIR"
# Generate list of checksums
sha512sum * > SHA512SUMS.chk 
# Check checksums
sha512sum -c SHA512SUMS.chk

# Sign checksum list 
gpg --detach-sign SHA512SUMS.chk 
gpg --verify SHA512SUMS.chk.sig SHA512SUMS.chk