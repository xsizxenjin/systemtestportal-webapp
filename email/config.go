/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package email

import "strconv"

// The configuration of the mail-server
// that is used to send mails
var configuration = getDefaultConfig()

// Configuration contains the information
// of the mail-server.
type Configuration struct {
	Hostname string `name:"smtp-hostname"`
	Port     int    `name:"smtp-port"`
}

// GetConfiguration returns the configuration
// of the mail-server
func GetConfiguration() Configuration {
	return configuration
}

// SetConfiguration sets the configuration of the
// mail-server
func SetConfiguration(config Configuration) {
	configuration = config
}

// GetAddress returns the address of the
// server in the format "hostname:port"
func (c Configuration) GetAddress() string {
	return c.Hostname + ":" + strconv.Itoa(c.Port)
}

// getDefaultConfig returns the default configuration
// of the mail server.
//
// The hostname is empty and the port is set to 25.
func getDefaultConfig() Configuration {
	return Configuration{
		Hostname: "",
		Port:     25,
	}
}
