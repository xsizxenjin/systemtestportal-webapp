/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package email

// authentication is the authentication to the mail-server
// that is configured with the "Configuration"
var authentication = Authentication{
	Username: "",
	Password: "",
}

// Authentication is used to authenticate
// with "Username" and "Password" to the
// "Hostname" of the Configuration.
type Authentication struct {
	Username string `name:"smtp-username"`
	Password string `name:"smtp-password"`
}

// GetAuthentication returns the authentication
// for the mail-server
func GetAuthentication() Authentication {
	return authentication
}

// SetAuthentication sets the authentication
// for the mail-server
func SetAuthentication(auth Authentication) {
	authentication = auth
}
