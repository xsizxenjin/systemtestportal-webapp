/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

//---------------------------------------------------------------------------------------------------------------------|
// TODO: Insert File Description Here
//---------------------------------------------------------------------------------------------------------------------|

package activityItems

import (
	"bytes"
	"html/template"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/contextdomain"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

//---------------------------------------------------------------------------------------------------------------------|
// STRUCTS
//---------------------------------------------------------------------------------------------------------------------|

type TestProtocol struct {
	// Automated Xorm Fields -----------|
	Id        int64
	UpdatedAt time.Time `xorm:"updated"`
	// Foreign Keys --------------------|
	TestProtocolId int64
	TestId         int64
	// ---------------------------------|

	IsCase bool

	CaseExecutionProtocol     *test.CaseExecutionProtocol     `xorm:"-"`
	SequenceExecutionProtocol *test.SequenceExecutionProtocol `xorm:"-"`

	// The test sequence/case are lazy loaded so we have more information in our activity item
	TestCase     *test.Case     `xorm:"-"`
	TestSequence *test.Sequence `xorm:"-"`
}

//---------------------------------------------------------------------------------------------------------------------|
// FUNCTIONS
//---------------------------------------------------------------------------------------------------------------------|

func (testProtocol TestProtocol) RenderItemTemplate(activity *activity.Activity, lang string) (template.HTML, error) {
	tmpl := getActivityItemBaseTree(lang).Append(templates.TestProtocol).Get().Lookup("testprotocol")

	b := &bytes.Buffer{}
	data, err := dict("Activity", activity, "SystemSettings", contextdomain.GetGlobalSystemSettings())
	if err != nil {
		return "", err
	}
	if err := tmpl.Execute(b, data); err != nil {
		return "", err
	}

	return template.HTML(b.String()), nil
}
