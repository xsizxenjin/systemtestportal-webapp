/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
)

var stepPrt1 = StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 4, 0),
	ObservedBehavior: "",
	Result:           NotAssessed,
	Comment:          "",
	Visited:          true,
}
var stepPrt2 = StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 10, 0),
	ObservedBehavior: "",
	Result:           Pass,
	Comment:          "Hello?",
	Visited:          true,
}
var stepPrt3 = StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 22, 0),
	ObservedBehavior: "Something happened",
	Result:           PartiallySuccessful,
	Comment:          "What's up?",
	Visited:          true,
}
var stepPrt4 = StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 0, 0),
	ObservedBehavior: "",
	Result:           NotAssessed,
	Comment:          "",
	Visited:          true,
}
var stepPrt5 = StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 0, 0),
	ObservedBehavior: "",
	Result:           NotAssessed,
	Comment:          "",
	Visited:          true,
}
var stepPrt6 = StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 1, 0),
	ObservedBehavior: "",
	Result:           NotAssessed,
	Comment:          "",
	Visited:          true,
}
