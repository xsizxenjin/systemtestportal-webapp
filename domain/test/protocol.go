/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"sync"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

var usedIDs = make(map[id.TestVersionID]int)
var usedIDsMux sync.Mutex

// ProtocolLister is used to list protocols for a given testVersion.
type ProtocolLister interface {
	// GetTestVersionProtocols gets the protocols for the testVersion with given id
	GetTestVersionProtocols(testVersionID id.TestVersionID) ([]id.ProtocolID, error)
	// GetCaseExecutionProtocol gets the caseProtocol related to the protocolID
	GetCaseExecutionProtocol(protocolID id.ProtocolID) (CaseExecutionProtocol, error)
}

//getProtocolNr returns a new id, which is unique for the given test version
//the new, returned id will be blocked and will not be returned again
func GetProtocolNr(rl ProtocolLister, testVersion id.TestVersionID) (int, error) {
	usedIDsMux.Lock()
	defer usedIDsMux.Unlock()

	highestID := usedIDs[testVersion]
	if highestID > 0 {
		usedIDs[testVersion] = highestID + 1
		return highestID + 1, nil
	}

	protocols, err := rl.GetTestVersionProtocols(testVersion)
	if err != nil {
		return -1, err
	}

	pID := 0
	for _, prtID := range protocols {
		if prtID.Protocol > pID {
			pID = prtID.Protocol
		}
	}

	usedIDs[testVersion] = pID + 1
	return pID + 1, nil
}
