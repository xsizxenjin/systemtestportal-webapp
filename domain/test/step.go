/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

// The Step struct contains information for describing a single step of a test case
type Step struct {
	Action         string
	ExpectedResult string
	Index          int
}

// NewTestStep creates a new Step with action and expected result from parameters
func NewTestStep(action, expect string) Step {
	s := Step{action, expect, 0}
	return s
}

// ExampleSteps is slice of exemplary Step-slices for use in tests and initialization
var ExampleSteps = [][]Step{{
	Step{"Type \"SystemTestPortal\" into the search bar and press enter.",
		"The search results for \"SystemTestPortal\" appear in a list.", 1},
	Step{"Click on the menu-button (The three horizontal lines at the top right corner).",
		"The sidebar opens.", 2},
	Step{"Click on the third theme (the blue-green one).",
		"The user interface design changes." +
			"The URL addresses are displayed beneath the title of the search result. " +
			"The URL font color changes to green. The icons before the URL disappear. " +
			"The font gets bold.", 3},
}, {
	Step{"Type \"SystemTestPortal\" into the search bar and press enter.",
		"\"SystemTestPortal\" is written into the search bar. The search results are listed. " +
			"Under the search bar \"Web\" is selected as search result type.", 1},
	Step{"Click on the search result that leads to systemtestportal.org.",
		"www.systemtestportal.org opens.", 2},
}, {
	Step{"Type \"portal image\" into the search bar and press enter.",
		"A new layout opens with multiple images. " +
			"Under the search bar \"Images\" is selected as search result type.", 1},
	Step{"Under the row to select the search result type there is a second row of filters. " +
		"Click on the last filter which says \"All Colors\" and select \"Black and White\".",
		"Instead of \"All Color\" there stands now \"Black and White\". " +
			"The search results change. All search results are predominantly black and white.", 2},
}, {
	Step{"Type \"test definition\" into the search bar and press enter.",
		"Under the search bar \"Definition\" is selected as search result type. " +
			"On the top is a highlighted area with definitions for \"test\". " +
			"Several search results of websites follow.", 1},
}, {
	Step{"Click on the menu-button (The three horizontal lines at the top right corner).",
		"The sidebar opens.", 1},
	Step{"Select \"other Settings\".",
		"A list of adjustable settings is viewed.", 2},
	Step{"Select \"Español de España\" in the drop down menu on the right of \"Language\".",
		"The site is translated into Spanish (e.g. Settings changes into \"Ajustes\").", 3},
	Step{"Scroll to the end of the settings list and click \"Guardar y salir\".",
		"The site changes back to DuckDuckGo.com. " +
			"The language viewed here is the language you selected in your browser settings.", 4},
}, {
	Step{"Click on the arrow down in the bottom center.",
		"The page scrolls down.", 1},
	Step{"Click on the button \"Add DuckDuckGo to Chrome\".",
		"The notification \"Add DuckDuckGo for Chrome?\" appears.", 2},
	Step{"Click \"Add extension\". ",
		"The notification \"DuckDuckGo for Chrome has been added to Chrome...\" appears. " +
			"In the top right corner the icon of DuckDuckGo is viewed.", 3},
	Step{"Press ALT + G.",
		"In the top right corner a pop up appears with a search bar inside.", 4},
}, {
	Step{"Click on the menu-button (The three horizontal lines at the top right corner).",
		"The sidebar opens.", 1},
	Step{"Select \"other Settings\".",
		"A list of adjustable settings is viewed.", 2},
	Step{"Click \"Appearance\" in the list on the right of the headline \"Settings\". ",
		"A list of other adjustable settings is viewed.", 3},
	Step{"Select \"Largest\" in the drop down menu on the right of \"Font Size\".",
		"The font size increases.", 4},
	Step{"Scroll to the end of the settings list and click \"Save and Exit\".",
		"The site changes back to DuckDuckGo.com.", 5},
}, {
	Step{"Type \"Stuttgart Map\" into the search bar.",
		"The search results are viewed. At the top is a small map showing stuttgart." +
			" A list of websites follows.", 1},
	Step{"Select \"OpenStreetMap\" in the drop down in the bottom right corner of the map.",
		"OpenStreetMap is written next to the small triangle of the drop down menu.", 2},
	Step{"Click on the map.",
		"The map opens in full screen mode.", 3},
	Step{"Click on \"Directions\".",
		"OpenStreetMap opens. The coordinates 48.7761, 9.1775 are entered as the finish of the route.",
		4},
}, {
	Step{"Type \"s\" into the search bar.",
		"No list of auto-suggestions appears under the search bar.", 1},
	Step{"Click on the menu-button (The three horizontal lines at the top right corner).",
		"The sidebar opens.", 2},
	Step{"Select \"other Settings\".",
		"A list of adjustable settings is viewed.", 3},
	Step{"Click the button \"Off\" next to \"Auto-Suggest\".",
		"The color of the button changes. The description of the button changes to \"On\".", 4},
	Step{"Scroll to the end of the settings list and click \"Save and Exit\".",
		"The site changes back to DuckDuckGo.com.", 5},
	Step{"Type \"s\" into the search bar.",
		"A list of auto-suggestions appears under the search bar.", 6},
}, {
	Step{"Type \"Pizza recipes\" into the search bar and press enter.",
		"\"Pizza recipes\" is written into the search bar. The search results are listed. " +
			"Under the search bar \"Recipes\" is selected as search result type.", 1},
}, {
	Step{"Click on the arrow down in the bottom center.",
		"The page scrolls down.", 1},
	Step{"Click on the button \"Add DuckDuckGo to Firefox\".",
		"The notification \"Firefox blocked this website....\" appears.", 2},
	Step{"Click \"Permit\".",
		"The notification \"Add DuckDuckGo Plus? ...\" appears.", 3},
	Step{"Click \"Add\".",
		"The notification \"DuckDuckGo for Chrome has been added to Chrome...\" appears. " +
			"In the top right corner the icon of DuckDuckGo is viewed.", 4},
	Step{"Press ALT + G.",
		"In the top right corner a pop up appears with a search bar inside.", 5},
}, {
	Step{"Click on the arrow down in the bottom center.",
		"The page scrolls down.", 1},
	Step{"Click on the button \"Add DuckDuckGo to Edge\".",
		"The site \"Take Back Your Privacy! ...\" is displayed.", 2},
	Step{"Follow the instructions on this site. Open a new tap and type \"SystemTestPortal\" " +
		"into the search bar.",
		"The search engine you used was DuckDuckGo (i.e the URL in the search bar " +
			"starts with \"https://duckduckgo.com/\").", 3},
}}
