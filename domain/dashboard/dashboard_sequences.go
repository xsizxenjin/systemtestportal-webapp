/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package dashboard

import (
	"sort"
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type Dashboard_Sequences struct {
	Project           *project.Project
	Sequences         []*test.Sequence
	Versions          []*project.Version
	Variants          []project.Variant
	ProtocolMap       map[id.TestID][]test.SequenceExecutionProtocol
	DashboardElements []DashboardElement_Sequences
}

type DashboardElement_Sequences struct {
	Versions []*project.Version
	Variant  project.Variant
	Results  []SequenceResult
}

type SequenceResult struct {
	TestSequence *test.Sequence
	Results      []test.Result
	Protocols    []test.SequenceExecutionProtocol
}

func NewDashboard_Sequences(proj *project.Project, sequences []*test.Sequence, protocolMap map[id.TestID][]test.SequenceExecutionProtocol) Dashboard_Sequences {

	dashboardElements := make([]DashboardElement_Sequences, 0)
	versions := make([]*project.Version, 0)

	var variants = getAllVariantsOfProject(proj)
	variants = SortVariants(variants)

	for _, version := range proj.Versions {
		versions = append(versions, version)
	}
	SortVersions(versions)

	for _, variant := range variants {
		dashboardElements = append(dashboardElements, NewDashboardElement_Sequences(sequences, versions, variant, protocolMap))
	}
	sortDashboardElements_Sequences(dashboardElements)

	return Dashboard_Sequences{
		Project:           proj,
		Sequences:         sequences,
		Versions:          versions,
		Variants:          variants,
		DashboardElements: dashboardElements,
		ProtocolMap:       protocolMap,
	}
}

func NewDashboardElement_Sequences(sequences []*test.Sequence, versions []*project.Version, variant project.Variant, protocolMap map[id.TestID][]test.SequenceExecutionProtocol) DashboardElement_Sequences {

	results := GetResults_Sequences(sequences, protocolMap, versions, variant)

	return DashboardElement_Sequences{
		Versions: versions,
		Variant:  variant,
		Results:  results,
	}
}

// GetCaseResultForVariant_Sequences returns the latest results for all versions of the protocols
func GetCaseResultForVariant_Sequences(testSequence *test.Sequence, protocols []test.SequenceExecutionProtocol, versions []*project.Version, variant project.Variant) SequenceResult {

	results := make([]test.Result, 0)
	var sequenceProtocols []test.SequenceExecutionProtocol

	for _, version := range versions {
		if _, ok := testSequence.SequenceVersions[0].SequenceInfo.Versions[version.Name]; ok &&
			test.ContainsVariant(testSequence.SequenceVersions[0].SequenceInfo.Versions[version.Name].Variants, variant) {
			sequenceProtocols = append(sequenceProtocols, getLatestTestSequenceProtocolForVersion(protocols, version, variant))
			results = append(results, getLatestResult_Sequences(protocols, version, variant))
		} else {
			results = append(results, test.NotApplicable)
		}
	}

	return SequenceResult{
		TestSequence: testSequence,
		Results:      results,
		Protocols:    sequenceProtocols,
	}
}

func getLatestResult_Sequences(protocols []test.SequenceExecutionProtocol, version *project.Version, variant project.Variant) test.Result {
	var result test.Result
	var latest time.Time

	for _, vari := range version.Variants {
		if vari == variant {
			for _, protocol := range protocols {
				if protocol.SUTVersion == version.Name && protocol.SUTVariant == vari.Name {
					if protocol.ExecutionDate.After(latest) {
						result = protocol.Result
						latest = protocol.ExecutionDate
					}
				}
			}
		}
	}

	return result
}

func GetResults_Sequences(sequences []*test.Sequence, protocolMap map[id.TestID][]test.SequenceExecutionProtocol, versions []*project.Version, variant project.Variant) []SequenceResult {
	results := make([]SequenceResult, 0)
	// iterate over sequences and append results where applicable
	for _, testSequence := range sequences {
		results = append(results, GetCaseResultForVariant_Sequences(testSequence, protocolMap[testSequence.ID()], versions, variant))
	}
	return results
}

func sortDashboardElements_Sequences(elements []DashboardElement_Sequences) {
	sort.Slice(elements, func(i, j int) bool {
		cmp := strings.Compare(elements[i].Variant.Name, elements[j].Variant.Name)
		switch cmp {
		case 0:
			return false
		case 1:
			return false
		case -1:
			return true
		default:
			return false
		}
	})
}

func getLatestTestSequenceProtocolForVersion(protocols []test.SequenceExecutionProtocol, version *project.Version, variant project.Variant) test.SequenceExecutionProtocol {
	var latest time.Time
	var retProtocol test.SequenceExecutionProtocol

	for _, vari := range version.Variants {
		if vari == variant {
			for _, protocol := range protocols {
				if protocol.SUTVersion == version.Name && protocol.SUTVariant == vari.Name {
					if protocol.ExecutionDate.After(latest) {
						retProtocol = protocol
						latest = protocol.ExecutionDate
					}
				}
			}
		}
	}
	return retProtocol
}
