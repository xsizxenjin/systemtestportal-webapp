/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

import (
	"encoding/json"
	"fmt"
	"net/http"

	"strconv"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

const (
	protocolKey = "Protocol"
)

// ProtocolExistenceChecker provides everything needed to check, if a protocol is already existent in storage.
type ProtocolExistenceChecker interface {
	//Exists checks, if the given protocol is already existent in storage.
	Exists(id ProtocolID) (bool, error)
}

// nonPositiveProtocolNr returns an ErrorMessage detailing that a given ProtocolNr is null or negative, but shouldn't.
func nonPositiveProtocolNr() error {
	return errors.ConstructStd(http.StatusBadRequest, "Invalid ProtocolNr",
		fmt.Sprintf("The given ProtocolNr is null or negative, but shouldn't."), nil).
		WithLog("The ProtocolNr, send by the client, is null or negative, but shouldn't..").
		WithStackTrace(2).
		Finish()
}

//ProtocolID hold all needed data to identify the a protocol in the whole system.
//This is the number of the protocol and the test version that was executed.
//Protocol can be related to a case verison or a sequence version
type ProtocolID struct {
	TestVersionID
	Protocol int
}

//NewProtocolID returns a new ProtocolID containing the given protocol number and related test version.
//No validation will be performed.
func NewProtocolID(testVersion TestVersionID, protocol int) ProtocolID {
	return ProtocolID{testVersion, protocol}
}

//Validate checks if this is a valid id for a new protocol.
func (id ProtocolID) Validate(pec ProtocolExistenceChecker) error {
	if id.Protocol <= 0 {
		return nonPositiveProtocolNr()
	}
	exists, err := pec.Exists(id)
	if err != nil {
		return err
	}
	if exists {
		return alreadyExistsErr(id)
	}
	return nil
}

//toStrings is a helper function for encoding. Returns all fields as a string slice
func (id *ProtocolID) toStrings() []string {
	return []string{
		id.Owner(),
		id.Project(),
		id.Test(),
		strconv.FormatBool(id.IsCase()),
		strconv.Itoa(id.TestVersion()),
		strconv.Itoa(id.Protocol),
	}
}

//fromStrings is a helper function for decoding. Fills the fields with the information given in the string slice
func (id *ProtocolID) fromStrings(strings []string) (err error) {
	id.ActorID = ActorID(strings[0])
	id.project = strings[1]
	id.test = strings[2]
	id.isCase, err = strconv.ParseBool(strings[3])
	if err != nil {
		return
	}
	id.testVersion, err = strconv.Atoi(strings[4])
	if err != nil {
		return
	}
	id.Protocol, err = strconv.Atoi(strings[5])
	return

}

//GobEncode encodes the id to a byte stream so it can be stored for example to a session
func (id *ProtocolID) GobEncode() ([]byte, error) {
	idString := strings.Join(id.toStrings(), "?")
	return []byte(idString), nil
}

//GobDecode decodes the id from a byte stream. All fields of the id called on will be overwritten
func (id *ProtocolID) GobDecode(data []byte) (err error) {
	idString := string(data)
	ids := strings.Split(idString, "?")
	return id.fromStrings(ids)
}

//MarshalJSON will marshal the id into json
func (id ProtocolID) MarshalJSON() ([]byte, error) {
	idStrings := id.toStrings()

	ID := make(map[string]string)
	ID[ownerKey] = idStrings[0]
	ID[projectKey] = idStrings[1]
	ID[testKey] = idStrings[2]
	ID[testCaseKey] = idStrings[3]
	ID[testVersionKey] = idStrings[4]
	ID[protocolKey] = idStrings[5]

	return json.Marshal(&ID)
}

//UnmarshalJSON will unmarshal the id from a given json. All fields of the id called on will be overwritten
func (id *ProtocolID) UnmarshalJSON(data []byte) (err error) {
	ID := make(map[string]string)
	err = json.Unmarshal(data, &ID)
	if err != nil {
		return
	}

	idStrings := make([]string, 6)
	idStrings[0] = ID[ownerKey]
	idStrings[1] = ID[projectKey]
	idStrings[2] = ID[testKey]
	idStrings[3] = ID[testCaseKey]
	idStrings[4] = ID[testVersionKey]
	idStrings[5] = ID[protocolKey]

	err = id.fromStrings(idStrings)
	return
}
