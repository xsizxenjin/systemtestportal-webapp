-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
--
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.


-- +migrate Up
UPDATE test_case_protocols SET result = replace(result, 'PassWithComment', 'PartiallySuccessful');
UPDATE test_sequence_protocols SET result = replace(result, 'PassWithComment', 'PartiallySuccessful');
UPDATE test_case_step_protocols SET result = replace(result, 'PassWithComment', 'PartiallySuccessful');


-- +migrate Down
UPDATE test_case_protocols SET result = replace(result, 'PartiallySuccessful', 'PassWithComment');
UPDATE test_sequence_protocols SET result = replace(result, 'PartiallySuccessful', 'PassWithComment');
UPDATE test_case_step_protocols SET result = replace(result, 'PartiallySuccessful', 'PassWithComment');