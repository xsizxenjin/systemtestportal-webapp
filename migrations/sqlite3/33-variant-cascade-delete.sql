-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
--
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up

PRAGMA foreign_keys = "0";
PRAGMA defer_foreign_keys = "1";

CREATE TABLE test_case_version_project_variants_new (
    id INTEGER PRIMARY KEY,
    test_case_version INTEGER NOT NULL REFERENCES test_case_versions(id) ON DELETE CASCADE,
    project_variant INTEGER NOT NULL REFERENCES project_variants(id) ON DELETE CASCADE,
    CONSTRAINT uniq_test_case_version_project_variant UNIQUE (test_case_version, project_variant)
);

INSERT INTO test_case_version_project_variants_new  SELECT * FROM test_case_version_project_variants; 

DROP TABLE test_case_version_project_variants;

CREATE TABLE test_case_version_project_variants (
id INTEGER PRIMARY KEY,
    test_case_version INTEGER NOT NULL REFERENCES test_case_versions(id) ON DELETE CASCADE,
    project_variant INTEGER NOT NULL REFERENCES project_variants(id) ON DELETE CASCADE,
    CONSTRAINT uniq_test_case_version_project_variant UNIQUE (test_case_version, project_variant)
);

INSERT INTO test_case_version_project_variants  SELECT * FROM test_case_version_project_variants_new; 


DROP TABLE test_case_version_project_variants_new;

PRAGMA foreign_keys = "1";
PRAGMA defer_foreign_keys = "0";


-- +migrate Down

PRAGMA foreign_keys = "0";
PRAGMA defer_foreign_keys = "1";

CREATE TABLE test_case_version_project_variants_new (
    id INTEGER PRIMARY KEY,
    test_case_version INTEGER NOT NULL REFERENCES test_case_versions(id) ON DELETE CASCADE,
    project_variant INTEGER NOT NULL REFERENCES project_variants(id) ON DELETE CASCADE,
    CONSTRAINT uniq_test_case_version_project_variant UNIQUE (test_case_version, project_variant)
);

INSERT INTO test_case_version_project_variants_new  SELECT * FROM test_case_version_project_variants; 

DROP TABLE test_case_version_project_variants;

CREATE TABLE test_case_version_project_variants (
id INTEGER PRIMARY KEY,
    test_case_version INTEGER NOT NULL REFERENCES test_case_versions(id) ON DELETE CASCADE,
    project_variant INTEGER NOT NULL REFERENCES project_variants(id),
    CONSTRAINT uniq_test_case_version_project_variant UNIQUE (test_case_version, project_variant)
);

INSERT INTO test_case_version_project_variants  SELECT * FROM test_case_version_project_variants_new; 


DROP TABLE test_case_version_project_variants_new;

PRAGMA foreign_keys = "1";
PRAGMA defer_foreign_keys = "0";