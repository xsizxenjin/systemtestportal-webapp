-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
PRAGMA defer_foreign_keys = "1";
ALTER TABLE project_versions RENAME TO temp_project_versions;
ALTER TABLE project_variants RENAME TO temp_project_variants;
ALTER TABLE test_case_version_project_versions RENAME TO temp_test_case_version_project_versions;
PRAGMA defer_foreign_keys = "0";

CREATE TABLE project_versions (
    id INTEGER PRIMARY KEY,
    project_id INTEGER NOT NULL REFERENCES projects(id) ON DELETE CASCADE,
    name VARCHAR(255) NOT NULL,
    CONSTRAINT uniq_project_id_name UNIQUE (project_id, name)
);
CREATE TABLE project_variants (
    id INTEGER PRIMARY KEY, 
    version_id INTEGER NOT NULL REFERENCES project_versions(id) ON DELETE CASCADE, 
    name VARCHAR(255) NOT NULL,
    CONSTRAINT uniq_version_id_name UNIQUE (version_id, name)
);

CREATE TABLE test_case_version_project_variants (
    id INTEGER PRIMARY KEY,
    test_case_version INTEGER NOT NULL REFERENCES test_case_versions(id) ON DELETE CASCADE,
    project_variant INTEGER NOT NULL REFERENCES project_variants(id),
    CONSTRAINT uniq_test_case_version_project_variant UNIQUE (test_case_version, project_variant)
);

INSERT INTO project_versions(id, project_id, name)
SELECT id, project_id, name FROM temp_project_variants;

INSERT INTO project_variants(id, version_id, name) 
SELECT id, variant_id, name FROM temp_project_versions;

PRAGMA defer_foreign_keys = "1";

INSERT INTO test_case_version_project_variants(id, test_case_version, project_variant) 
SELECT id, test_case_version, project_version FROM temp_test_case_version_project_versions;


DROP TABLE temp_project_variants;
DROP TABLE temp_project_versions;
DROP TABLE temp_test_case_version_project_versions;

PRAGMA defer_foreign_keys = "0";

-- +migrate Down
PRAGMA defer_foreign_keys = "1";
ALTER TABLE project_variants RENAME TO temp_project_variants; 
ALTER TABLE project_versions RENAME TO temp_project_versions;
ALTER TABLE test_case_version_project_variants RENAME TO temp_test_case_version_project_variants;
PRAGMA defer_foreign_keys = "0";
CREATE TABLE project_variants (
    id INTEGER PRIMARY KEY,
    project_id INTEGER NOT NULL REFERENCES projects(id) ON DELETE CASCADE,
    name VARCHAR(255) NOT NULL,
    CONSTRAINT uniq_project_id_name UNIQUE (project_id, name)
);
CREATE TABLE project_versions (
    id INTEGER PRIMARY KEY, 
    version_id INTEGER NOT NULL REFERENCES project_versions(id) ON DELETE CASCADE, 
    name VARCHAR(255) NOT NULL,
    CONSTRAINT uniq_version_id_name UNIQUE (version_id, name)
);

CREATE TABLE test_case_version_project_versions (
    id INTEGER PRIMARY KEY,
    test_case_version INTEGER NOT NULL REFERENCES test_case_versions(id) ON DELETE CASCADE,
    project_version INTEGER NOT NULL REFERENCES project_versions(id),
    CONSTRAINT uniq_test_case_version_project_version UNIQUE (test_case_version, project_version)
);

PRAGMA defer_foreign_keys = "1";

INSERT INTO project_variants(id, project_id, name)
SELECT id, project_id, name FROM temp_project_variants;

INSERT INTO project_versions(id, version_id, name) 
SELECT id, variant_id, name FROM temp_project_versions;

INSERT INTO test_case_version_project_versions(id, test_case_version, project_version) 
SELECT id, test_case_version, project_variant FROM temp_test_case_version_project_variants;

DROP TABLE temp_project_variants;
DROP TABLE temp_project_versions;
DROP TABLE temp_test_case_version_project_variants;
PRAGMA defer_foreign_keys = "0";