-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
--
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
ALTER TABLE todo_items ADD COLUMN type INTEGER NOT NULL DEFAULT 0;

-- +migrate Down
ALTER TABLE todo_items RENAME TO todo_items_old;
CREATE TABLE todo_items (
    id INTEGER PRIMARY KEY,
    list_id INTEGER NOT NULL REFERENCES todo_lists(id) ON DELETE CASCADE,
    todo_item_index INTEGER NOT NULL,
    author_id INTEGER NOT NULL REFERENCES users(id),
    project_id INTEGER NOT NULL REFERENCES projects(id) ON DELETE CASCADE,
    reference_id INTEGER NOT NULL,
    reference_type INTEGER NOT NULL,
    deadline TIMESTAMP,
    creation_date TIMESTAMP,
    done BIT(1),
    CONSTRAINT uniq_todo_item_index_list_id UNIQUE (todo_item_index, list_id)
);

INSERT INTO todo_items SELECT id, list_id, todo_item, author_id, project_id, reference_id, reference_type, deadline, creation_date, done FROM todo_items_old;
DROP TABLE todo_items_old;