/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package sessions

import (
	"fmt"
	"net/http"
	"time"

	"encoding/gob"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	storePac "gitlab.com/stp-team/systemtestportal-webapp/store"
)

const userSessionName = "User-Session"
const recSessionName = "Protocol-Session"
const idKey = "ID"
const caseRecKey = "Case Protocol"
const seqRecKey = "Sequence Protocol"
const durKey = "DurationTotal"
const cookieAge = 1209600 //2 weeks

type userDAO interface {
	Get(id id.ActorID) (*user.User, bool, error)
}

func init() {
	gob.Register(&test.CaseExecutionProtocol{})
	gob.Register(&test.SequenceExecutionProtocol{})
	gob.Register(&duration.Duration{})
	gob.Register(id.ActorID(""))
}

//Store provides functions to store and get data to the session of a request
//Don't create your own Store! Better get the global Store via the corresponding function
type Store struct {
	store    sessions.Store
	users    userDAO
	bootTime time.Time
}

//store is the global Store
var store Store

//GetSessionStore returns a pointer to the session-store
func GetSessionStore() *Store {
	return &store
}

//InitSessionManagement initialize the global Store.
//Default parameters are nil, anything else is just for testing
func InitSessionManagement(s sessions.Store, us userDAO) {
	//bootTime
	store = Store{nil, nil, time.Now().UTC()}
	//store
	if s == nil {
		store.store = sessions.NewCookieStore(securecookie.GenerateRandomKey(64))
	} else {
		store.store = s
	}
	//userDAO
	if us == nil {
		store.users = storePac.GetUserStore()
	} else {
		store.users = us
	}
}

func (s *Store) getDatedSessionName(name string) string {
	return fmt.Sprintf(
		"%s%d%d%d%d%d",
		name,
		s.bootTime.Year(),
		s.bootTime.YearDay(),
		s.bootTime.Hour(),
		s.bootTime.Minute(),
		s.bootTime.Second(),
	)
}

func (s *Store) getUserSessionName() string {
	return s.getDatedSessionName(userSessionName)
}

func (s *Store) getRecSessionName() string {
	return s.getDatedSessionName(recSessionName)
}

//GetCurrent returns the current signed in user to the given request.
//If the current user is not signed in, the function will return nil, nil
//If an error occurs nil and the error will be returned
func (s *Store) GetCurrent(r *http.Request) (*user.User, error) {
	session, err := s.store.Get(r, s.getUserSessionName())
	if err != nil {
		return nil, err
	}
	userID, ok := session.Values[idKey]
	if !ok {
		return nil, nil
	}
	currentUser, ok, err := s.users.Get(userID.(id.ActorID))
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, fmt.Errorf("couldn't resolve user with id %v", userID)
	}
	return currentUser, nil
}

//StartFor connects the session of the request with an user.
//After this call, you can get the user via the GetCurrent-function
func (s *Store) StartFor(w http.ResponseWriter, r *http.Request, user *user.User) error {
	session, err := s.store.Get(r, s.getUserSessionName())
	if err != nil {
		return err
	}
	session.Options = &sessions.Options{MaxAge: cookieAge}
	session.Values[idKey] = user.ID()
	err = session.Save(r, w)
	return err
}

//EndFor removes the user from the session
//User parameter is not used by now, just for future
func (s *Store) EndFor(w http.ResponseWriter, r *http.Request, u *user.User) error {
	session, err := s.store.Get(r, s.getUserSessionName())
	if err != nil {
		return err
	}
	session.Options = &sessions.Options{MaxAge: -1}
	err = session.Save(r, w)
	return err
}

//SetCurrentCaseProtocol saves the given protocol to the session.
//After this call, you can get the current case protocol via the GetCurrentCaseProtocol-function
func (s *Store) SetCurrentCaseProtocol(w http.ResponseWriter, r *http.Request,
	protocol *test.CaseExecutionProtocol) error {

	return s.set(w, r, s.getRecSessionName(), caseRecKey, protocol)
}

//RemoveCurrentCaseProtocol removes the current case protocol from the session.
func (s *Store) RemoveCurrentCaseProtocol(w http.ResponseWriter, r *http.Request) error {
	return s.remove(w, r, s.getRecSessionName(), caseRecKey)
}

func (s *Store) remove(w http.ResponseWriter, r *http.Request, sessionName string, id interface{}) error {
	session, err := s.store.Get(r, sessionName)
	if err != nil {
		return err
	}
	session.Options = &sessions.Options{MaxAge: cookieAge}
	delete(session.Values, id)
	err = session.Save(r, w)
	return err
}

func (s *Store) set(w http.ResponseWriter, r *http.Request, sessionName string,
	id interface{}, value interface{}) error {
	session, err := s.store.Get(r, sessionName)
	if err != nil {
		return err
	}
	session.Options = &sessions.Options{MaxAge: cookieAge}
	session.Values[id] = value
	return session.Save(r, w)
}

// getSessionValue gets a value form the session with given name, using given key.
func (s *Store) getSessionValue(r *http.Request, name string, key interface{}) (interface{}, error) {
	session, err := s.store.Get(r, name)
	if err != nil {
		return nil, err
	}
	rec, ok := session.Values[key]
	if ok {
		return rec, nil
	}
	return err, nil
}

// TestStore is a Test-Stub of Session-Store
type TestStore struct {
	session *sessions.Session
}

// Get gets the session with given name from given request.
func (s *TestStore) Get(r *http.Request, name string) (*sessions.Session, error) {
	if s.session == nil {
		return s.New(r, name)
	}
	return s.session, nil
}

// New creates a new session with given name for given request.
func (s *TestStore) New(r *http.Request, name string) (*sessions.Session, error) {
	return sessions.NewSession(s, name), nil
}

// Save saves a session and writes into the response writer.
func (s *TestStore) Save(r *http.Request, w http.ResponseWriter,
	session *sessions.Session) error {
	if session.Options != nil && session.Options.MaxAge < 0 {
		s.session = nil
	} else {
		s.session = session
	}
	return nil
}
