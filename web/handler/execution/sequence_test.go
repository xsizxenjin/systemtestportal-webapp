/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"encoding/json"
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/store/dummydata"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestSequenceStartPageGet(t *testing.T) {
	// Invalid case version in request parameters
	paramsInvalidVersion := url.Values{}
	paramsInvalidVersion.Add(httputil.Version, "invalidversion")

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProject,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUser,
		},
	)
	ctxNoProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      nil,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUser,
		},
	)
	ctxNoSequence := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProject,
			middleware.TestSequenceKey: nil,
			middleware.UserKey:         handler.DummyUser,
		},
	)
	ctxUnauthorized := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProject,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUserUnauthorized,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Unauthorized user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				timeSession := &handler.TimeSessionMock{}
				caseProtocolGetter := &handler.CaseProtocolGetterMock{}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				return SequenceStartPageGet(caseProtocolGetter), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
					handler.HasCalls(timeSession, 0),
					handler.HasCalls(caseProtocolGetter, 0),
					handler.HasCalls(sequenceSessionGetter, 0),
				)
			},
			handler.SimpleRequest(ctxUnauthorized, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorized, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No project in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				timeSession := &handler.TimeSessionMock{}
				caseProtocolGetter := &handler.CaseProtocolGetterMock{}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				return SequenceStartPageGet(caseProtocolGetter), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(timeSession, 0),
					handler.HasCalls(caseProtocolGetter, 0),
					handler.HasCalls(sequenceSessionGetter, 0),
				)
			},
			handler.SimpleRequest(ctxNoProject, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No sequence in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				timeSession := &handler.TimeSessionMock{}
				caseProtocolGetter := &handler.CaseProtocolGetterMock{}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				return SequenceStartPageGet(caseProtocolGetter), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(timeSession, 0),
					handler.HasCalls(caseProtocolGetter, 0),
					handler.HasCalls(sequenceSessionGetter, 0),
				)
			},
			handler.SimpleRequest(ctxNoSequence, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoSequence, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Invalid sequence version",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				timeSession := &handler.TimeSessionMock{}
				caseProtocolGetter := &handler.CaseProtocolGetterMock{}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				return SequenceStartPageGet(caseProtocolGetter), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(timeSession, 0),
					handler.HasCalls(caseProtocolGetter, 0),
					handler.HasCalls(sequenceSessionGetter, 0),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, paramsInvalidVersion),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, paramsInvalidVersion),
		),
		handler.CreateTest("Normal request",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				timeSession := &handler.TimeSessionMock{}
				caseProtocolGetter := &handler.CaseProtocolGetterMock{}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				return SequenceStartPageGet(caseProtocolGetter), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(timeSession, 0),
					handler.HasCalls(caseProtocolGetter, 0),
					handler.HasCalls(sequenceSessionGetter, 0),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestSequenceExecutionPost(t *testing.T) {
	// Params for testing the response executionPageNotFound
	paramsInvalidStep := url.Values{}
	paramsInvalidStep.Add(keyStepNr, "-1")

	paramsFinishedSequence := url.Values{}
	paramsFinishedSequence.Add(keyCaseNr, "0")
	paramsFinishedSequence.Add(keyStepNr, "1")

	paramsRedirect := url.Values{}
	paramsRedirect.Add(keyCaseNr, "3")
	paramsRedirect.Add(keyStepNr, "1")

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProject,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUser,
		},
	)
	ctxNoProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      nil,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUser,
		},
	)
	ctxNoSequence := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProject,
			middleware.TestSequenceKey: nil,
			middleware.UserKey:         handler.DummyUser,
		},
	)
	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProject,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         nil,
		},
	)
	ctxUnauthorized := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProject,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUserUnauthorized,
		},
	)

	handler.Suite(t,
		handler.CreateTest("No project in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseProtocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				sequenceProtocolAdder := &handler.SequenceProtocolAdderMock{}

				caseGetter := &handler.CaseGetterMock{}
				taskGetterMock := &handler.TaskListGetterMock{}
				taskAdderMock := &handler.TaskListAdderMock{}
				activityStoreMock := &handler.ActivityMock{}


				issueGetterMock := &handler.IssueGetterMock{}
				issueAdderMock := &handler.IssueAdderMock{}

				return SequenceExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, caseGetter, nil, taskAdderMock, taskGetterMock, activityStoreMock,
					issueGetterMock, issueAdderMock), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(ctxNoProject, http.MethodPost, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoProject, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("No user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseProtocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				sequenceProtocolAdder := &handler.SequenceProtocolAdderMock{}
				taskGetterMock := &handler.TaskListGetterMock{}
				taskAdderMock := &handler.TaskListAdderMock{}
				activityStoreMock := &handler.ActivityMock{}

				caseGetter := &handler.CaseGetterMock{}
				issueGetterMock := &handler.IssueGetterMock{}
				issueAdderMock := &handler.IssueAdderMock{}
				return SequenceExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, caseGetter, nil, taskAdderMock, taskGetterMock, activityStoreMock,
					issueGetterMock, issueAdderMock), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(ctxNoUser, http.MethodPost, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoUser, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("No sequence in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseProtocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				sequenceProtocolAdder := &handler.SequenceProtocolAdderMock{}
				taskGetterMock := &handler.TaskListGetterMock{}
				taskAdderMock := &handler.TaskListAdderMock{}
				activityStoreMock := &handler.ActivityMock{}

				caseGetter := &handler.CaseGetterMock{}
				issueGetterMock := &handler.IssueGetterMock{}
				issueAdderMock := &handler.IssueAdderMock{}
				return SequenceExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, caseGetter, nil, taskAdderMock, taskGetterMock, activityStoreMock,
					issueGetterMock, issueAdderMock), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(ctxNoSequence, http.MethodPost, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoSequence, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseProtocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				sequenceProtocolAdder := &handler.SequenceProtocolAdderMock{}
				taskGetterMock := &handler.TaskListGetterMock{}
				taskAdderMock := &handler.TaskListAdderMock{}
				activityStoreMock := &handler.ActivityMock{}

				caseGetter := &handler.CaseGetterMock{}
				issueGetterMock := &handler.IssueGetterMock{}
				issueAdderMock := &handler.IssueAdderMock{}
				return SequenceExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, caseGetter, nil, taskAdderMock, taskGetterMock, activityStoreMock,
					issueGetterMock, issueAdderMock), handler.Matches(
						handler.HasStatus(http.StatusForbidden),
					)
			},
			handler.SimpleRequest(ctxUnauthorized, http.MethodPost, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorized, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("Invalid step nr",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseProtocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				sequenceProtocolAdder := &handler.SequenceProtocolAdderMock{}
				taskGetterMock := &handler.TaskListGetterMock{}
				taskAdderMock := &handler.TaskListAdderMock{}
				activityStoreMock := &handler.ActivityMock{}

				caseGetter := &handler.CaseGetterMock{}
				issueGetterMock := &handler.IssueGetterMock{}
				issueAdderMock := &handler.IssueAdderMock{}
				return SequenceExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, caseGetter, nil, taskAdderMock, taskGetterMock, activityStoreMock,
					issueGetterMock, issueAdderMock), handler.Matches(
						handler.HasStatus(http.StatusNotFound),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsInvalidStep),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsInvalidStep),
		),
		handler.CreateTest("Redirect after showing sequence",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseProtocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				sequenceProtocolAdder := &handler.SequenceProtocolAdderMock{}
				taskGetterMock := &handler.TaskListGetterMock{}
				taskAdderMock := &handler.TaskListAdderMock{}
				activityStoreMock := &handler.ActivityMock{}

				caseGetter := &handler.CaseGetterMock{}
				issueGetterMock := &handler.IssueGetterMock{}
				issueAdderMock := &handler.IssueAdderMock{}
				return SequenceExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, caseGetter, nil, taskAdderMock, taskGetterMock, activityStoreMock,
					issueGetterMock, issueAdderMock), handler.Matches(
						handler.HasStatus(http.StatusSeeOther),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsFinishedSequence),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsFinishedSequence),
		),
		handler.CreateTest("Redirect",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseProtocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				sequenceProtocolAdder := &handler.SequenceProtocolAdderMock{}
				taskGetterMock := &handler.TaskListGetterMock{}
				taskAdderMock := &handler.TaskListAdderMock{}
				activityStoreMock := &handler.ActivityMock{}

				caseGetter := &handler.CaseGetterMock{}
				issueGetterMock := &handler.IssueGetterMock{}
				issueAdderMock := &handler.IssueAdderMock{}
				return SequenceExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, caseGetter, nil, taskAdderMock, taskGetterMock, activityStoreMock,
					issueGetterMock, issueAdderMock), handler.Matches(
						handler.HasStatus(http.StatusSeeOther),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsRedirect),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsRedirect),
		),
	)
}

func TestSequenceExecutionStartPost(t *testing.T) {
	paramsStartPage := url.Values{}
	paramsStartPage.Add(keyStepNr, "0")
	paramsStartPage.Add(keyCaseNr, "0")
	paramsStartPage.Add(keySUTVariant, "SUT-Variant")
	paramsStartPage.Add(keySUTVersion, "SUT-Version")
	paramsStartPage.Add("preconditions", "[\"fulfilled\"]")

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProject,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUser,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Valid request",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseProtocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				sequenceProtocolAdder := &handler.SequenceProtocolAdderMock{}
				taskGetterMock := &handler.TaskListGetterMock{}
				taskAdderMock := &handler.TaskListAdderMock{}
				activityStoreMock := &handler.ActivityMock{}

				caseGetter := &handler.CaseGetterMock{}
				issueGetterMock := &handler.IssueGetterMock{}
				issueAdderMock := &handler.IssueAdderMock{}
				return SequenceExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, caseGetter, nil, taskAdderMock, taskGetterMock, activityStoreMock,
					issueGetterMock, issueAdderMock), handler.Matches(
						handler.HasStatus(http.StatusOK),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsStartPage),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsStartPage),
		),
	)
}

func TestSequenceExecutionSummaryPost(t *testing.T) {
	seqProt := test.SequenceExecutionProtocol{
		TestVersion: dummydata.Sequences[0].SequenceVersions[0].ID(),
		CaseExecutionProtocols: []id.ProtocolID{
			{
				TestVersionID: handler.DummyTestSequence.SequenceVersions[0].ID(),
			},
			{
				TestVersionID: handler.DummyTestSequence.SequenceVersions[0].ID(),
			},
		},
	}
	sequenceBytes, _ := json.Marshal(seqProt)

	caseBytes, _ := json.Marshal(&test.CaseExecutionProtocol{
		TestVersion: dummydata.Cases[0].TestCaseVersions[0].ID(),
		StepProtocols: []test.StepExecutionProtocol{
			{},
			{},
			{}},
		Result: 0,
	})

	paramsCaseStart := url.Values{}
	paramsCaseStart.Add(keyStepNr, "0")
	paramsCaseStart.Add(keyCaseNr, "1")
	paramsCaseStart.Add(keySeqProtocol, string(sequenceBytes))
	paramsCaseStart.Add("duration", "{\"hours\":0,\"minutes\":0,\"seconds\":15}")
	paramsCaseStart.Add(keyCaseProtocol, string(caseBytes))

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProject,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUser,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Valid request end of sequence execution",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseProtocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				sequenceProtocolAdder := &handler.SequenceProtocolAdderMock{}
				taskGetterMock := &handler.TaskListGetterMock{}
				taskAdderMock := &handler.TaskListAdderMock{}
				activityStoreMock := &handler.ActivityMock{}

				caseGetter := &handler.CaseGetterMock{}
				issueGetterMock := &handler.IssueGetterMock{}
				issueAdderMock := &handler.IssueAdderMock{}
				return SequenceExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, caseGetter, nil, taskAdderMock, taskGetterMock, activityStoreMock,
					issueGetterMock, issueAdderMock), handler.Matches(
						handler.HasStatus(http.StatusOK),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsCaseStart),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsCaseStart),
		),
	)
}
