/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

// caseProgress is the progress meter for the execution of a
// single testcase.
type caseProgress struct {
	cur, max int
}

func (p *caseProgress) Init(r *http.Request, previous bool) error {
	c := handler.GetContextEntities(r)
	if c.Case == nil {
		return c.Err
	}

	tcv, err := handler.GetTestCaseVersion(r, c.Case)
	if err != nil {
		return err
	}
	//
	if previous {
		p.cur = getFormValueInt(r, keyStepNr) + 1
	} else {
		p.cur = getFormValueInt(r, keyStepNr) + 2
	}

	p.max = len(tcv.Steps) + 3
	return nil
}

func (p *caseProgress) Add(progress int) {
	p.cur += progress
}

func (p *caseProgress) Get() int {
	return p.cur
}

func (p *caseProgress) Max() int {
	return p.max
}
