/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// SystemSettingsGet is the handler that displays the SystemSettings site.
func SystemSettingsGet(w http.ResponseWriter, r *http.Request) {
	tmpl := getSystemSettingsTree(r)
	handler.PrintTmpl(context.New().WithUserInformation(r), tmpl, w, r)
}

// getSystemSettinhsTree returns the systemsettings template with all parent templates
func getSystemSettingsTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// System Settings tree
		Append(templates.SystemSettings).
		Get().Lookup(templates.HeaderDef)
}
