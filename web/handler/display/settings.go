/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/modal"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

var templatesSettingsAll = []string{
	templates.ErrorModal,
	templates.Settings,
	templates.GenericError,
	templates.DeleteRole,
	templates.DeleteConfirm,
}

var templatesSettingsProject = []string{
	templates.SettingsNavProjects,
	templates.SettingsTabProjects,
}

var templatesSettingsPermissions = []string{
	templates.SettingsNavPermissions,
	templates.SettingsTabPermissions,
}

var templatesSettingsIntegrations = []string{
	templates.SettingsNavIntegrations,
	templates.SettingsTabIntegrations,
}

func ensureContextAndPermissions(request *http.Request) (*handler.ContextEntities, error) {

	c := handler.GetContextEntities(request)
	if c.Project == nil || c.ContainerID == "" {
		return nil, c.Err
	}

	if c.User == nil {
		return nil, handler.UnauthorizedAccess(request)
	}

	// Owner always has the right to access the settings. This prevents the members from
	// locking themselves out from editing the project and the roles
	if !c.Project.IsOwner(c.User) && !c.Project.GetPermissions(c.User).EditProject && !c.User.IsAdmin {
		return nil, handler.UnauthorizedAccess(request)
	}

	return c, nil
}

// GetSettingsProjects is a handler for showing the project settings page
func GetSettingsProjects(writer http.ResponseWriter, request *http.Request) {
	c, err := ensureContextAndPermissions(request)
	if err != nil {
		errors.Handle(err, writer, request)
		return
	}

	templ := getSettingsProjectsTemplates(request)
	handler.PrintTmpl(context.New().
		WithUserInformation(request).
		With(context.Project, c.Project).
		With(context.DeleteProject, modal.ProjectDeleteMessage).
		With(context.SettingsIdentifier, "Project"), templ, writer, request)
}

// GetSettingsPermissions is a handler for showing the permissions settings page
func GetSettingsPermissions(writer http.ResponseWriter, request *http.Request) {
	c, err := ensureContextAndPermissions(request)
	if err != nil {
		errors.Handle(err, writer, request)
	}

	templ := getSettingsPermissionsTemplates(request)
	handler.PrintTmpl(context.New().
		WithUserInformation(request).
		With(context.Project, c.Project).
		With(context.DeleteProject, modal.ProjectDeleteMessage).
		With(context.SettingsIdentifier, "Permission"), templ, writer, request)
}

// GetSettingsIntegrations is a handler for showing the integration settings page
func GetSettingsIntegrations(writer http.ResponseWriter, request *http.Request) {
	c, err := ensureContextAndPermissions(request)
	if err != nil {
		errors.Handle(err, writer, request)
	}

	templ := getSettingsIntegrationsTemplates(request)
	handler.PrintTmpl(context.New().
		WithUserInformation(request).
		With(context.Project, c.Project).
		With(context.SettingsIdentifier, "Integrations"), templ, writer, request)
}

func getSettingsProjectsTemplates(request *http.Request) *template.Template {
	if httputil.IsFragmentRequest(request) {
		return handler.GetBaseTree(request).
			Append(templatesSettingsAll...).
			Append(templatesSettingsProject...).
			Get().Lookup(templates.TabContent)
	}
	return handler.GetNoSideBarTree(request).
		Append(templates.ContentProjectTabs).
		Append(templatesSettingsAll...).
		Append(templatesSettingsProject...).
		Get().Lookup(templates.HeaderDef)
}

func getSettingsPermissionsTemplates(request *http.Request) *template.Template {
	if httputil.IsFragmentRequest(request) {
		return handler.GetBaseTree(request).
			Append(templatesSettingsAll...).
			Append(templatesSettingsPermissions...).
			Get().Lookup(templates.TabContent)
	}
	return handler.GetNoSideBarTree(request).
		Append(templates.ContentProjectTabs).
		Append(templatesSettingsAll...).
		Append(templatesSettingsPermissions...).
		Get().Lookup(templates.HeaderDef)
}

func getSettingsIntegrationsTemplates(request *http.Request) *template.Template {
	if httputil.IsFragmentRequest(request) {
		return handler.GetBaseTree(request).
			Append(templatesSettingsAll...).
			Append(templatesSettingsIntegrations...).
			Get().Lookup(templates.TabContent)
	}
	return handler.GetNoSideBarTree(request).
		Append(templates.ContentProjectTabs).
		Append(templatesSettingsAll...).
		Append(templatesSettingsIntegrations...).
		Get().Lookup(templates.HeaderDef)
}

func GetAsyncSettingsProjectsNav(writer http.ResponseWriter, request *http.Request) {
	c, err := ensureContextAndPermissions(request)
	if err != nil {
		errors.Handle(err, writer, request)
	}

	templ := handler.GetBaseTree(request).
		Append(templates.SettingsNavProjects).
		Get().Lookup(templates.SettingsNav)
	handler.PrintTmpl(context.New().
		WithUserInformation(request).
		With(context.Project, c.Project).
		With(context.SettingsIdentifier, "Project"), templ, writer, request)
}

func GetAsyncSettingsProjectsTab(writer http.ResponseWriter, request *http.Request) {
	c, err := ensureContextAndPermissions(request)
	if err != nil {
		errors.Handle(err, writer, request)
	}

	templ := handler.GetBaseTree(request).
		Append(templates.SettingsTabProjects, templates.GenericError, templates.DeleteConfirm).
		Get().Lookup(templates.SettingsTab)
	handler.PrintTmpl(context.New().
		WithUserInformation(request).
		With(context.Project, c.Project).
		With(context.DeleteProject, modal.ProjectDeleteMessage).
		With(context.SettingsIdentifier, "Permission"), templ, writer, request)
}

func GetAsyncSettingsPermissionsNav(writer http.ResponseWriter, request *http.Request) {
	c, err := ensureContextAndPermissions(request)
	if err != nil {
		errors.Handle(err, writer, request)
	}

	templ := handler.GetBaseTree(request).
		Append(templates.SettingsNavPermissions).
		Get().Lookup(templates.SettingsNav)
	handler.PrintTmpl(context.New().
		WithUserInformation(request).
		With(context.Project, c.Project).
		With(context.SettingsIdentifier, "Permission"), templ, writer, request)
}

func GetAsyncSettingsPermissionsTab(writer http.ResponseWriter, request *http.Request) {
	c, err := ensureContextAndPermissions(request)
	if err != nil {
		errors.Handle(err, writer, request)
	}

	templ := handler.GetBaseTree(request).
		Append(templates.SettingsTabPermissions, templates.DeleteRole).
		Get().Lookup(templates.SettingsTab)
	handler.PrintTmpl(context.New().
		WithUserInformation(request).
		With(context.Project, c.Project).
		With(context.SettingsIdentifier, "Permission"), templ, writer, request)
}

func GetAsyncSettingsIntegrationsNav(writer http.ResponseWriter, request *http.Request) {
	c, err := ensureContextAndPermissions(request)
	if err != nil {
		errors.Handle(err, writer, request)
	}

	templ := handler.GetBaseTree(request).
		Append(templates.SettingsNavIntegrations).
		Get().Lookup(templates.SettingsNav)
	handler.PrintTmpl(context.New().
		WithUserInformation(request).
		With(context.Project, c.Project).
		With(context.SettingsIdentifier, "Integrations"), templ, writer, request)
}

func GetAsyncSettingsIntegrationsTab(writer http.ResponseWriter, request *http.Request) {
	c, err := ensureContextAndPermissions(request)
	if err != nil {
		errors.Handle(err, writer, request)
	}

	templ := handler.GetBaseTree(request).
		Append(templates.SettingsTabIntegrations, templates.GenericError).
		Get().Lookup(templates.SettingsTab)
	handler.PrintTmpl(context.New().
		WithUserInformation(request).
		With(context.Project, c.Project).
		With(context.SettingsIdentifier, "Integrations"), templ, writer, request)
}
