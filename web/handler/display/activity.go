/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"
	"strconv"

	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

func ActivityGet(activityStore handler.Activities) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		contextEntities := handler.GetContextEntities(request)

		if contextEntities.Project == nil {
			errors.Handle(contextEntities.Err, writer, request)
			return
		}

		activities, err := activityStore.GetActivitiesForProjectLimitFromOffset(contextEntities.Project.Id, 20, 0)
		if err != nil {
			errors.Handle(err, writer, request)
			return
		}

		lang := httputil.GetLanguageFromRequest(request)

		tmpl := getProjectActivityFragment(request)

		handler.PrintTmpl(context.New().
			WithUserInformation(request).
			With(context.Project, contextEntities.Project).
			With(context.Activities, activities).
			With(context.Language, lang),
			tmpl, writer, request)
	}
}

func ActivityPut(activityStore handler.Activities) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		contextEntities := handler.GetContextEntities(request)

		if contextEntities.Project == nil {
			errors.Handle(contextEntities.Err, writer, request)
			return
		}

		offsetCount, err := strconv.Atoi(request.FormValue(httputil.OffsetCount))
		if err != nil {
			errors.Handle(err, writer, request)
			return
		}

		activities, err := activityStore.GetActivitiesForProjectLimitFromOffset(contextEntities.Project.Id, 20, 20*offsetCount)
		if err != nil {
			errors.Handle(err, writer, request)
			return
		}

		if len(activities) == 0 {
			errors.Handle(errors.Construct(http.StatusBadRequest).Finish(), writer, request)
		}

		lang := httputil.GetLanguageFromRequest(request)

		var activityItemHTMLCollection template.HTML
		for _, activity := range activities {
			activityItemHTML := activity.GetRenderedItemTemplate(lang)
			activityItemHTMLCollection = activityItemHTMLCollection + activityItemHTML
		}

		writer.Write([]byte(activityItemHTMLCollection))
	}
}

func getProjectActivityFragment(request *http.Request) *template.Template {
	if httputil.IsFragmentRequest(request) {
		return getTabActivityFragment(request)
	}
	return getTabActivityTree(request)
}

func getTabActivityTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		Append(templates.ContentProjectTabs).
		Append(templates.Activity).
		Get().Lookup(templates.HeaderDef)
}

func getTabActivityFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.Activity).
		Get().Lookup(templates.TabContent)
}
