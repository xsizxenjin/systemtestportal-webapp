/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"encoding/json"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

const (
	errCouldNotDecodeRolesTitle = "Couldn't save roles."
	errCouldNotDecodeRoles      = "We were unable to decode the change to roles " +
		"send in your request. This ist most likely a bug. If you want please " +
		"contact us via our " + handler.IssueTracker + "."
)

type roleInput struct {
	Names       []string
	Permissions [][]bool
}

// ProjectRolesPut handles the request to update the
// roles of a project
func ProjectRolesPut(projectAdder handler.ProjectAdder) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		input := roleInput{}

		if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeRolesTitle, errCouldNotDecodeRoles, r).
				WithLog("Couldn't read labels from request.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		for i, name := range input.Names {
			if c.Project.Roles[project.RoleName(name)] != nil {
				c.Project.Roles[project.RoleName(name)].Permissions.Execute = input.Permissions[i][0]
				c.Project.Roles[project.RoleName(name)].Permissions.CreateCase = input.Permissions[i][1]
				c.Project.Roles[project.RoleName(name)].Permissions.EditCase = input.Permissions[i][2]
				c.Project.Roles[project.RoleName(name)].Permissions.DeleteCase = input.Permissions[i][3]
				c.Project.Roles[project.RoleName(name)].Permissions.DuplicateCase = input.Permissions[i][4]
				c.Project.Roles[project.RoleName(name)].Permissions.AssignCase = input.Permissions[i][5]
				c.Project.Roles[project.RoleName(name)].Permissions.CreateSequence = input.Permissions[i][6]
				c.Project.Roles[project.RoleName(name)].Permissions.EditSequence = input.Permissions[i][7]
				c.Project.Roles[project.RoleName(name)].Permissions.DeleteSequence = input.Permissions[i][8]
				c.Project.Roles[project.RoleName(name)].Permissions.DuplicateSequence = input.Permissions[i][9]
				c.Project.Roles[project.RoleName(name)].Permissions.AssignSequence = input.Permissions[i][10]
				c.Project.Roles[project.RoleName(name)].Permissions.EditMembers = input.Permissions[i][11]
				c.Project.Roles[project.RoleName(name)].Permissions.EditProject = input.Permissions[i][12]
				c.Project.Roles[project.RoleName(name)].Permissions.DeleteProject = input.Permissions[i][13]
				c.Project.Roles[project.RoleName(name)].Permissions.EditPermissions = input.Permissions[i][14]
				c.Project.Roles[project.RoleName(name)].Permissions.DisplayProject = input.Permissions[i][15]
			} else {
				perm := project.Permissions{
					project.DisplayPermissions{
						DisplayProject: input.Permissions[i][15],
					},
					project.ExecutionPermissions{
						Execute: input.Permissions[i][0],
					},
					project.CasePermissions{
						CreateCase:    input.Permissions[i][1],
						EditCase:      input.Permissions[i][2],
						DeleteCase:    input.Permissions[i][3],
						AssignCase:    input.Permissions[i][4],
						DuplicateCase: input.Permissions[i][5],
					},
					project.SequencePermissions{
						CreateSequence:    input.Permissions[i][6],
						EditSequence:      input.Permissions[i][7],
						DeleteSequence:    input.Permissions[i][8],
						AssignSequence:    input.Permissions[i][9],
						DuplicateSequence: input.Permissions[i][10],
					},
					project.MemberPermissions{
						EditMembers: input.Permissions[i][11],
					},
					project.SettingsPermissions{
						EditProject:     input.Permissions[i][12],
						DeleteProject:   input.Permissions[i][13],
						EditPermissions: input.Permissions[i][14],
					},
				}
				role := project.Role{
					Name:        project.RoleName(name),
					Permissions: perm,
				}
				var r *project.Role
				r = &role
				c.Project.Roles[project.RoleName(name)] = r
			}
		}

		if err := projectAdder.Add(c.Project); err != nil {
			errors.Handle(err, w, r)
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}
