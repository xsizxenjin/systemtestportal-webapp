/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"time"

	"reflect"

	"github.com/nicksnyder/go-i18n/i18n"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/contextdomain"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/store/storeuploads"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
	"gitlab.com/stp-team/systemtestportal-webapp/web/util"
)

// IssueTracker is a html link to the issue board of the stp-repository
const IssueTracker = "<a href='https://gitlab.com/stp-team/systemtestportal-webapp/issues'>issue tracker</a>"

const (
	errPrintTemplateTitle = "We were unable to print the page you requested!"
	errPrintTemplate      = "We are sorry, but we were unable to build the page " +
		"you wanted to see. This is most likely a bug. " +
		"Please contact us via our " + IssueTracker + "."
)

// PrintTmpl prints given template with given context into given writer.
// If an error occurs, an error response is written to the writer instead
// of the executed template's content.
//
// If the given writer was a http.ResponseWriter the status code
// http.StatusInternalServerError will be written to its header if an
// error occurred.
func PrintTmpl(ctx context.Context, template *template.Template, w io.Writer, r *http.Request) {

	b := &bytes.Buffer{}

	settings := contextdomain.GetGlobalSystemSettings()
	var ul UserLister
	ul = store.GetUserStore()

	users, err := ul.List()
	if err != nil {
		fmt.Errorf("error: %s", err)
	}
	userkey := r.Context().Value(middleware.UserKey)
	signedIn := false
	for _, user := range users {

		if reflect.DeepEqual(user, userkey) {
			signedIn = true
			break
		}
	}

	if !signedIn && (!settings.IsAccessAllowed && r.URL.Path != "/register" && r.URL.Path != "/about" && r.URL.Path != "/imprint" && r.URL.Path != "/privacy" && r.URL.Path != "/license") {
		//Replace Template with public login page
		//Redirect User to public login page
		template = GetNoSideBarTree(r).Append(templates.Public).Get().Lookup(templates.HeaderDef)
	}
	err = template.Execute(b, ctx)
	if err == nil {
		_, err = b.WriteTo(w)
	}
	if err != nil {
		tmplErr := errors.ConstructStd(http.StatusInternalServerError,
			errPrintTemplateTitle, errPrintTemplate, r).
			WithLogf("Unable to print template %q.", template.Name()).
			WithStackTrace(2).
			WithCause(err).
			Finish()
		if rw, ok := w.(http.ResponseWriter); ok {
			errors.Respond(tmplErr, rw)
		} else {
			errors.Log(tmplErr)
			_, err = fmt.Fprint(w, tmplErr.Body())
			if err != nil {
				log.Panicf("Couldn't write error body to writer. "+
					"Error body was:\n%s", tmplErr.Body())
			}
		}
	}
}

func getUserImageFromName(username string) string {
	var ul UserLister
	ul = store.GetUserStore()

	users, err := ul.List()
	if err != nil {
		fmt.Errorf("error: %s", err)
		return "/static/img/profile-placeholder.png"
	}
	var targetUser *user.User = nil
	for _, user := range users {
		if user.Name == username {
			targetUser = user
			break
		}
	}
	if targetUser.Image == "" {
		return "/static/img/profile-placeholder.png"
	} else {
		return targetUser.Image
	}
}

/* Returns the imagepath for the given username */
func getUserImageFromID(id id.ActorID) string {
	var ul UserLister
	ul = store.GetUserStore()

	users, err := ul.List()
	if err != nil {
		fmt.Errorf("error: %s", err)
		return "/static/img/profile-placeholder.png"
	}
	var targetUser *user.User = nil
	for _, user := range users {
		if user.ID() == id {
			targetUser = user
			break
		}
	}
	if targetUser.Image == "" {
		return "/static/img/profile-placeholder.png"
	} else {
		return targetUser.Image
	}
}

func printMarkdown(s string) template.HTML {
	return template.HTML(util.ParseMarkdown(s))
}

func toIso8601Time(time time.Time) string {
	return time.UTC().Format("2006-01-02T15:04:05-0700")
}

// GetBaseTree returns a loaded base template containing
// only utility function used by the templates.
func GetBaseTree(r *http.Request) templates.LoadedTemplate {
	lang := httputil.GetLanguageFromRequest(r)

	T, _ := i18n.Tfunc(lang)

	defaultFuncMap := map[string]interface{}{
		"getUserImageFromID":   getUserImageFromID,
		"getUserImageFromName": getUserImageFromName,
		"printMarkdown":        printMarkdown,
		"getImagePath":         storeuploads.GetImagePath,
		"provideTimeago":       toIso8601Time,
		"dict":                 Dict,
		"T":                    T,
		"sha256":               httputil.GenerateShaForFile,
	}

	return templates.Load().Funcs(defaultFuncMap)
}

func Dict(values ...interface{}) (map[string]interface{}, error) {
	if len(values)%2 != 0 {
		return nil, fmt.Errorf("invalid dict call")
	}
	dict := make(map[string]interface{}, len(values)/2)
	for i := 0; i < len(values); i += 2 {
		key, ok := values[i].(string)
		if !ok {
			return nil, fmt.Errorf("dict keys must be strings")
		}
		dict[key] = values[i+1]
	}
	return dict, nil
}

// GetSideBarTree returns a loaded template structure for a
// view containing the sidebar.
func GetSideBarTree(r *http.Request) templates.LoadedTemplate {
	return GetBaseTree(r).Append(templates.Header, templates.Navbar, templates.Footer). // Navbar tree
		// Modal sign-in tree
		Append(templates.SignIn).
		// Sidebar tree
		Append(templates.ContentSidebar, templates.MenuExplore)
}

// GetPrintTree returns a loaded template structure for a
// print view .
func GetPrintTree(r *http.Request) templates.LoadedTemplate {
	return GetBaseTree(r).Append("static/header-print", "static/navbar-print", "static/footer-print"). // Navbar tree
		// Sidebar tree
		Append("static/content-print")
}

// GetNoSideBarTree returns a loaded template structure for a
// view not containing the sidebar.
func GetNoSideBarTree(r *http.Request) templates.LoadedTemplate {
	return GetBaseTree(r).Append(templates.Header, templates.Navbar, templates.Footer). // Navbar tree
		// Modal sign-in tree
		Append(templates.SignIn).
		// No sidebar tree
		Append(templates.ContentNoSidebar)
}
