/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package json

import (
	"net/http"
	"testing"

	"net/url"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestSequenceGet(t *testing.T) {
	// Reset name of dummy test sequence
	handler.DummyTestSequence.Name = "test-sequence-1"

	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: nil,
			middleware.UserKey:         handler.DummyUser,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUser,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	ctxPrivateProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUserUnauthorized,
			middleware.ProjectKey:      handler.DummyProjectPrivate,
		},
	)

	tested := SequenceGet
	handler.Suite(t,
		handler.CreateTest("Invalid context",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusInternalServerError)),
			handler.EmptyRequest(http.MethodGet),
		),
		handler.CreateTest("Nil as testsequence in context",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusInternalServerError)),
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No member of private project",
			handler.ExpectResponse(tested,
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Valid request",
			handler.ExpectResponse(tested,
				handler.HasStatus(http.StatusOK),
				handler.HasBody(handler.DummyTestSequenceJSON),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestSequencesGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)
	ctxPrivateProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUserUnauthorized,
			middleware.ProjectKey:      handler.DummyProjectPrivate,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.SequenceListerMock{}
				return SequencesGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No member of private project",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.SequenceListerMock{}
				return SequencesGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusForbidden),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.SequenceListerMock{}
				return SequencesGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
						handler.HasCalls(&mock, 1),
						handler.HasBody("[]"),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestSequenceInfoGet(t *testing.T) {
	params := url.Values{}
	params.Add(httputil.NewTestCases, "testcaseID1/testcaseID2/")
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)
	ctxPrivateProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:    handler.DummyUserUnauthorized,
			middleware.ProjectKey: handler.DummyProjectPrivate,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseGetter := handler.CaseGetterMock{}
				return SequenceInfoGet(&caseGetter),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(invalidCtx, http.MethodGet, params),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, params),
		),
		handler.CreateTest("Normal case no ids",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseGetter := handler.CaseGetterMock{}
				return SequenceInfoGet(&caseGetter),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No member of private project",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseGetter := handler.CaseGetterMock{}
				return SequenceInfoGet(&caseGetter),
					handler.Matches(
						handler.HasStatus(http.StatusForbidden),
					)
			},
			handler.SimpleRequest(ctxPrivateProject, http.MethodGet, params),
			handler.SimpleFragmentRequest(ctxPrivateProject, http.MethodGet, params),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseGetter := handler.CaseGetterMock{}
				return SequenceInfoGet(&caseGetter),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, params),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, params),
		),
	)
}
