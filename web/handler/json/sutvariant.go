/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package json

import (
	"encoding/json"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

// ProjectVariantsGet simply serves the variants of a project as json.
func ProjectVariantsGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Project == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).DisplayProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	b, err := json.Marshal(c.Project.Versions)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	_, err = w.Write(b)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
}

const (
	errCouldNotDecodeVariantsTitle = "Couldn't save variants."
	errCouldNotDecodeVariants      = "We were unable to decode the change to variants " +
		"send in your request. This ist most likely a bug. If you want please " +
		"contact us via our " + handler.IssueTracker + "."
)

// ProjectVariantsPost handles request that demand changes to the existing set of
// variants.
func ProjectVariantsPost(ps handler.ProjectAdder) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}
		// This map is needed to update the sut variants.
		// Directly decoding and storing the request in
		// the projects variants does not work if a
		// variant was deleted
		var versions = map[string]*project.Version{}

		if err := json.NewDecoder(r.Body).Decode(&versions); err != nil {
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeVariantsTitle, errCouldNotDecodeVariants, r).
				WithLog("Couldn't read variants from request.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		c.Project.Versions = versions
		if err := ps.Add(c.Project); err != nil {
			errors.Handle(err, w, r)
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}
