/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func newUserServerMock(getUUserOut *user.User, getUFoundOut bool, addUErrorOut error) *handler.RegisterServerMock {
	return &handler.RegisterServerMock{
		GetUserIDIn:     "",
		GetUserMailIn:   "",
		GetUserUserOut:  getUUserOut,
		GetUserFoundOut: getUFoundOut,
		AddUserUserIn:   nil,
		AddUserErrorOut: addUErrorOut}
}

// TestRegister_ServeHTTP test the register handlers serve function.
func TestRegister_ServeHTTP(t *testing.T) {
	userServer := newUserServerMock(nil, false, nil)
	testHandler := NewRegisterHandler(userServer)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)

	name := "robert1234"
	displayName := "KingRob"
	email := "rob@test.de"
	password := "secret"
	isAdmin := "false"

	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/register", nil)
	req = req.WithContext(ctx)

	form := url.Values{}
	form.Add(httputil.UserName, name)
	form.Add(httputil.DisplayName, displayName)
	form.Add(httputil.Email, email)
	form.Add(httputil.Password, password)
	form.Add(httputil.IsAdmin, isAdmin)

	req.Form = form

	testHandler.ServeHTTP(rec, req)

	if code := rec.Result().StatusCode; code != http.StatusCreated {
		t.Errorf("Wrong status code. Expected %d but was %d.", http.StatusCreated, code)
	}

	if rec.Header().Get("newAccount") != name {
		t.Errorf("Returned wrong Name to client. "+
			"Expected %q but was %q.", name, rec.Header().Get("newAccount"))
	}

	if u := userServer.AddUserUserIn; u.Name != name || u.DisplayName != displayName ||
		u.Email != email || u.Password != password {
		t.Errorf("Userdata wasn't saved correctly. "+
			"Expected %+v but %+v was saved", name, userServer.GetUserIDIn)
	}
}

// TestRegister_ServeHTTP_AlreadyAssigned tests the register handlers serve function if the username is already assigned
func TestRegister_ServeHTTP_AlreadyAssigned(t *testing.T) {
	userServer := newUserServerMock(&user.User{Name: "robert1234"}, true, nil)
	testHandler := NewRegisterHandler(userServer)
	accountName := "robert1234"
	displayName := "KingRob"
	email := "rob@test.de"
	password := "secret"
	isAdminString := "false"
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/register", nil)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)
	req = req.WithContext(ctx)

	form := url.Values{}
	form.Add(httputil.UserName, accountName)
	form.Add(httputil.DisplayName, displayName)
	form.Add(httputil.Email, email)
	form.Add(httputil.Password, password)
	form.Add(httputil.IsAdmin, isAdminString)
	req.Form = form

	testHandler.ServeHTTP(rec, req)

	if code := rec.Result().StatusCode; code != http.StatusConflict {
		t.Errorf("Wrong status code. Expected %d but was %d.", http.StatusConflict, code)
	}
}

// TestRegister_ServeHTTP_InternalError tests the register
// handlers serve function if the user-server had an internal error
func TestRegister_ServeHTTP_InternalError(t *testing.T) {
	userServer := newUserServerMock(nil, false, errors.New("server down"))
	testHandler := NewRegisterHandler(userServer)
	accountName := "Robert1234"
	displayName := "KingRob"
	email := "rob@test.de"
	password := "secret"
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/register", nil)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)
	req = req.WithContext(ctx)
	form := url.Values{}
	form.Add(httputil.UserName, accountName)
	form.Add(httputil.DisplayName, displayName)
	form.Add(httputil.Email, email)
	form.Add(httputil.Password, password)
	req.Form = form

	testHandler.ServeHTTP(rec, req)

	if code := rec.Result().StatusCode; code != http.StatusInternalServerError {
		t.Errorf("Wrong status code. Expected %d but was %d.", http.StatusInternalServerError, code)
	}
}
