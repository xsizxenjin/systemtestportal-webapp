/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package export

import (
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"net/http"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"io"
	"log"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"encoding/json"
	"bytes"
)

type ExportJsonProject struct {
	Project *project.Project
	Cases 	[]*test.Case
	Sequences []*test.Sequence
	Labels 	[]*project.Label
	TestLabels []*project.TestLabel
}

func ProjectJson (tcl handler.TestCaseLister, tsl handler.TestSequenceLister, labelStore handler.Labels) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		c := handler.GetContextEntities(r)

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		proj := c.Project
		cases := GetCases(tcl, proj)
		sequences := GetSequences(tsl, proj)
		labels, err := labelStore.GetLabelsForProject(c.Project)
		if err != nil {
			errors.Handle(err, w , r)
		}

		testLabels, err := labelStore.GetAllTestLabels()
		if err != nil {
			errors.Handle(err, w , r)
		}


		body, err := buildJsonForProject(proj, cases, sequences, labels, testLabels)
		if err != nil {
			errors.Handle(err,w,r)
			return
		}

		filename := c.Project.ID().Project() + "-STP_Project.json"
		w.Header().Set("Content-Disposition", "attachment; filename="+filename)
		w.Header().Set("Content-Type", r.Header.Get("Content-Type"))

		io.Copy(w, body)
	}
}

// Build Json file for teh current project
func buildJsonForProject(proj *project.Project,cases []*test.Case, sequences []*test.Sequence, labels []*project.Label,
		testLabels []*project.TestLabel) (io.Reader, error){
	temp := ExportJsonProject{Project:proj, Cases:cases, Sequences:sequences, Labels: labels, TestLabels:testLabels}
	b,err := json.MarshalIndent(temp,"","\t")
	return bytes.NewReader(b),err
}

// GetCases returns all cases of the project
func GetCases(caseLister handler.TestCaseLister, pj *project.Project) []*test.Case {
	cases, err := caseLister.List(pj.ID())
	if err != nil {
		log.Println(err)
	}
	return cases
}

// GetSequences returns all sequences of the project
func GetSequences(sequenceLister handler.TestSequenceLister, project *project.Project) []*test.Sequence {
	sequences, err := sequenceLister.List(project.ID())
	if err != nil {
		log.Println(err)
	}

	return sequences
}
