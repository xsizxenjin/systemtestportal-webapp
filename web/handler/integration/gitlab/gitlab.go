/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package gitlab

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/integration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/slugs"
)

type IssueBody struct {
	IID int
}

// CreateIssue creates an issue in gitlab if the test of the
// protocol was not successful.
//
// Returns an error if any occurred.
func CreateIssue(issueGetter handler.IssueGetter, issueAdder handler.IssueAdder,
	project *project.Project, caseVersion test.CaseVersion, protocol test.CaseExecutionProtocol) error {

	if !project.Integrations.GitlabProject.IsActive ||
		protocol.Result != test.Fail ||
		issueAlreadyExists(issueGetter, caseVersion, protocol) {
		return nil
	}

	url := "https://gitlab.com/api/v4/projects/" + project.Integrations.GitlabProject.ID + "/issues" +
		"?title=" + createIssueTitle(protocol) +
		"&description=" + createIssueDescription(caseVersion, protocol)

	request, err := http.NewRequest(http.MethodPost, url, nil)
	if err != nil {
		return err
	}
	request.Header.Set("PRIVATE-TOKEN", project.Integrations.GitlabProject.Token)

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return err
	}

	// Add issue to database
	var issueBody IssueBody
	if err := json.NewDecoder(response.Body).Decode(&issueBody); err != nil {
		return err
	}
	issue := integration.NewIssue(&protocol, issueBody.IID)

	return issueAdder.AddIssue(issue)
}

// issueAlreadyExists checks whether an issue already exists for the
// test-case/version/variant combination.
func issueAlreadyExists(issueGetter handler.IssueGetter, caseVersion test.CaseVersion, protocol test.CaseExecutionProtocol) bool {
	issue, err := issueGetter.GetIssue(caseVersion.Case, protocol.SUTVersion, protocol.SUTVariant)
	if err != nil {
		return false
	}

	if issue != nil {
		return true
	}

	return false
}

func createIssueTitle(protocol test.CaseExecutionProtocol) string {
	var title string
	if protocol.Comment != "" {
		title = protocol.Comment
	} else {
		title = protocol.TestVersion.Test()
	}
	return slugs.URLEncode("Test failed: " + title)
}

func createIssueDescription(caseVersion test.CaseVersion, protocol test.CaseExecutionProtocol) string {
	description := ""
	newLine := "  \n"

	description += "**Test \"" + protocol.TestVersion.Test() + "\" failed**"
	description += newLine
	description += "SUT-Version: *" + protocol.SUTVersion + "*"
	description += newLine
	description += "SUT-Variant: *" + protocol.SUTVariant + "*"
	description += newLine
	description += "Executed on *" + protocol.ExecutionDate.UTC().String() + "*"
	description += newLine
	description += newLine
	if !protocol.IsAnonymous {
		description += "Tester: *" + protocol.UserName + "*"
		description += newLine
	}
	description += "Comment from the tester: *" + protocol.Comment + "*"
	description += newLine
	description += newLine

	description += "**Preconditions**"
	description += newLine
	if len(protocol.PreconditionResults) < 1 {
		description += "*No preconditions were specified for the test*"
		description += newLine
	}
	for _, precondition := range protocol.PreconditionResults {
		description += precondition.Precondition.Content + ": *" + precondition.Result + "*"
		description += newLine
	}
	description += newLine

	description += "**Test steps**"
	description += newLine
	for i, step := range protocol.StepProtocols {
		description += strconv.Itoa(i+1) + ": " + caseVersion.Steps[i].Action
		description += newLine
		description += "Result: *" + step.Result.PrettyString() + "*"
		description += newLine
		if step.ObservedBehavior != "" {
			description += "Observed behavior: *" + step.ObservedBehavior + "*"
			description += newLine
		}
		if step.Comment != "" {
			description += "Comment: *" + step.Comment + "*"
			description += newLine
		}
	}

	description += newLine
	description += "This issue was automatically created by the SystemTestPortal."

	return slugs.URLEncode(description)
}
