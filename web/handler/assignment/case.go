/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package assignment

import (
	"encoding/json"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/task"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

// CasePut handles the assignment of a test case to multiple testers
func CasePut(userRetriever middleware.UserRetriever, taskListAdder handler.TaskListAdder,
	taskListGetter handler.TaskListGetter, taskGetter handler.TaskGetter, taskAdder handler.TaskAdder, activityStore handler.Activities) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).AssignCase {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		testers, err := getNewTesters(userRetriever, r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		if err := createTaskForTesters(r, taskListGetter, taskListAdder, c, testers, task.Case); err != nil {
			errors.Handle(err, w, r)
			return
		}

		doneTasks, err := getTasksToSetDone(r, c.Case.ID(), taskGetter)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		if err := setTasksDone(taskAdder, doneTasks); err != nil {
			errors.Handle(err, w, r)
			return
		}

		err = activityStore.LogActivity(activity.ASSIGN_CASE, c.GetActivityEntities(), r)
		if err != nil {
			errors.Handle(err, w, r)
		}
	}
}

// setTasksDone sets the given tasks as done
func setTasksDone(taskAdder handler.TaskAdder, taskItems []*task.Item) error {
	for _, taskItem := range taskItems {
		taskItem.Done = true
		err := taskAdder.AddItem(*taskItem)
		if err != nil {
			return err
		}
	}

	return nil
}

// getTasksToSetDone gets the assignee and the index for the tasks
// that are supposed to be deleted from the request and returns
// the task-items of the users with these indices.
func getTasksToSetDone(request *http.Request, testID id.TestID, taskGetter handler.TaskGetter) ([]*task.Item, error) {
	tasks, err := taskGetter.GetTasksForTest(testID)
	if err != nil {
		return nil, err
	}

	var deleteTasks map[string][]string
	err = json.Unmarshal([]byte(request.FormValue(httputil.DeleteTasks)), &deleteTasks)
	if err != nil {
		return nil, err
	}

	var tasksToDelete []*task.Item
	for _, t := range tasks {
		// Only delete task if the assignee and index is in the list of testers to delete
		if taskIndices, ok := deleteTasks[t.Assignee.Actor()]; ok {
			for _, taskIndex := range taskIndices {
				if taskIndex == strconv.Itoa(t.Index) {
					tasksToDelete = append(tasksToDelete, t)
				}
			}
		}
	}
	return tasksToDelete, nil
}

// getVersions gets the selected versions from the request
func getVersions(request *http.Request) ([]string, error) {
	var versions []string
	err := json.Unmarshal([]byte(request.FormValue(httputil.Versions)), &versions)
	if err != nil {
		return nil, err
	}

	return versions, nil
}

// getVariants gets the selected variants from the request
func getVariants(request *http.Request) ([]string, error) {
	var variants []string
	err := json.Unmarshal([]byte(request.FormValue(httputil.Variants)), &variants)
	if err != nil {
		return nil, err
	}

	return variants, nil
}

const (
	errNoSuchRefType    = "invalid reference type"
	errNoSuchRefTypeMsg = "the given reference type is not supported"
)

// createTaskForTesters creates tod o-items for every tester.
//
// The refType defines the type of the entity that is referenced
// by the to do-item.
//
// Returns an error if the to do-list for a test cannot be retrieved or
// if the updated to do-list cannot be saved.
func createTaskForTesters(r *http.Request, taskListGetter handler.TaskListGetter, taskListAdder handler.TaskListAdder,
	contextEntities *handler.ContextEntities, testers []*user.User, refType task.ReferenceType) error {

	versions, err := getVersions(r)
	if err != nil {
		return err
	}

	variants, err := getVariants(r)
	if err != nil {
		return err
	}

	for index, tester := range testers {
		taskList, err := taskListGetter.Get(tester.Name)
		if err != nil {
			return err
		}

		var refID string
		switch refType {
		case task.Case:
			refID = contextEntities.Case.Name
		case task.Sequence:
			refID = contextEntities.Sequence.Name
		default:
			return errors.ConstructStd(http.StatusInternalServerError, errNoSuchRefType, errNoSuchRefTypeMsg, r).
				WithStackTrace(1).
				Finish()
		}

		taskList = taskList.AddItem(tester.ID(), contextEntities.User.ID(), contextEntities.Project.ID(), task.Assignment, refType, refID,
			versions[index], variants[index], time.Time{})

		if err := taskListAdder.Add(*taskList); err != nil {
			return err
		}
	}

	return nil
}

// containsUser checks if a slice of users contains a user.
//
// It returns
// -1: Slice does NOT contain the user e
// i: Index of the element
func containsUser(s []*user.User, e *user.User) int {
	for i, a := range s {
		if reflect.DeepEqual(a, e) {
			return i
		}
	}
	return -1
}
