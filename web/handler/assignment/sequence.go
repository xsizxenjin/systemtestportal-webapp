/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package assignment

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/task"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

//SequencePut handles the assignment of a test case to multiple testers
func SequencePut(us middleware.UserRetriever, taskListAdder handler.TaskListAdder, taskListGetter handler.TaskListGetter,
	taskGetter handler.TaskGetter, taskAdder handler.TaskAdder, activityStore handler.Activities) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).AssignSequence {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		testers, err := getNewTesters(us, r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		if err := createTaskForTesters(r, taskListGetter, taskListAdder, c, testers, task.Sequence); err != nil {
			errors.Handle(err, w, r)
			return
		}

		doneTasks, err := getTasksToSetDone(r, c.Sequence.ID(), taskGetter)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		if err := setTasksDone(taskAdder, doneTasks); err != nil {
			errors.Handle(err, w, r)
			return
		}

		err = activityStore.LogActivity(activity.ASSIGN_SEQUENCE, c.GetActivityEntities(), r)
		if err != nil {
			errors.Handle(err, w, r)
		}
	}
}
