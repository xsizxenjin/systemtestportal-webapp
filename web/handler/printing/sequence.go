/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package printing

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// SequencesListGet handles the showing of the template to print all test cases
func SequencesListGet(lister handler.TestSequenceLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tss, err := lister.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tmpl := getPrintSequenceListTemplate(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Project, c.Project).
			With(context.TestSequences, tss), tmpl, w, r)
	}
}

// getPrintSequenceListTemplate returns either only the test sequences
// print fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getPrintSequenceListTemplate(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesListPrintFragment(r)
	}
	return getTabTestSequencesListPrintTree(r)
}

// getTabTestSequencesListPrintTree returns the test case print tab template with all parent templates
func getTabTestSequencesListPrintTree(r *http.Request) *template.Template {
	return handler.GetPrintTree(r).
		// Tab testcase history tree
		Append(templates.PrintSequences).
		Get().Lookup(templates.HeaderPrint)
}

// getTabTestSequencesListPrintFragment returns only the test case print template
func getTabTestSequencesListPrintFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).Append(templates.PrintSequences).Get().Lookup(templates.TabContent)
}

// SequenceGet serves the page for printing a testsequence.
func SequenceGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Project == nil || c.Sequence == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).DisplayProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	printTestSequence(c.Project, c.Sequence, w, r)
}

// printTestSequence tries to respond with the print page for a testsequence
// if an error occurs an error response is sent instead.
func printTestSequence(p *project.Project, ts *test.Sequence, w http.ResponseWriter, r *http.Request) {
	tmpl := getPrintSequenceTemplate(r)
	tsv, err := handler.GetTestSequenceVersion(r, ts)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	ctx := context.New().
		WithUserInformation(r).
		With(context.Project, p).
		With(context.TestSequence, ts).
		With(context.TestSequenceVersion, tsv)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getPrintSequenceTemplate returns either only the test sequence
// print fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getPrintSequenceTemplate(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencePrintFragment(r)
	}
	return getTabTestSequencePrintTree(r)
}

// getTabTestSequencePrintTree returns the test sequence print tab template with all parent templates
func getTabTestSequencePrintTree(r *http.Request) *template.Template {
	return handler.GetPrintTree(r).
		// Tab testcase history tree
		Append(templates.PrintSequence).
		Get().Lookup(templates.HeaderPrint)
}

// getTabTestSequencePrintFragment returns only the test sequence print template
func getTabTestSequencePrintFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).Append(templates.PrintSequence).Get().Lookup(templates.TabContent)
}
