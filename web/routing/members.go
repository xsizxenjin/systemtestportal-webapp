/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/deletion"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/list"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/update"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func registerMembersHandler(pg *contextGroup, n *negroni.Negroni) {
	userStore := store.GetUserStore()
	projectStore := store.GetProjectStore()
	taskStore := store.GetTaskStore()

	mcg := wrapContextGroup(pg.NewContextGroup(Members))

	mcg.HandlerGet(Show,
		n.With(negroni.WrapFunc(list.MembersGet(projectStore, userStore, userStore, taskStore))))
	mcg.HandlerPut(Add,
		n.With(negroni.WrapFunc(creation.MemberPut(userStore, projectStore))))
	mcg.HandlerDelete(Remove,
		n.With(negroni.WrapFunc(deletion.MemberDelete(userStore, projectStore))))
	mcg.HandlerPatch(Update,
		n.With(negroni.WrapFunc(update.MemberUpdate(userStore, projectStore))))

	registerMemberHandler(mcg, n)
}

func registerMemberHandler(tcsg *contextGroup, n *negroni.Negroni) {
	userStore := store.GetUserStore()
	taskStore := store.GetTaskStore()

	projectStore := store.GetProjectStore()

	tcg := wrapContextGroup(tcsg.NewContextGroup(Members))

	tcg.HandlerGet(Show, n.With(middleware.Project(projectStore),
		negroni.WrapFunc(list.MembersGet(projectStore, userStore, userStore, taskStore))))
}
